﻿FORMAT: 1A
HOST: https://turismo.software.com

# Turismo API

Esta documentación es un borrador que se presentará el miércoles para Software, se usó como base las instrucciones del libro [Build APIs You Won't Hate].

[Build APIs You Won't Hate]: https://leanpub.com/build-apis-you-wont-hate

## Authorization

La autorización se hará en base a un Java Web Token (JWT) que será firmado por el servidor y se lo enviará a cada usuario (En realidad se mandarán 2 JWT uno para realizar las transacciones y otro para hacer un refresh del primero en caso de que expire).
La estructura de los JWT será comenzando por el Token para realizar los requests:

```json
{
  "sub": 5,
  "iat": 1559099479,
  "exp": 1559100079,
  "type": "bearer"
}
```

Estructura Token para hacer refresh:

```json
{
  "sub": 5,
  "iat": 1559099479,
  "exp": 1559100979,
  "type": "refresh"
}
```

El tiempo de duración del token de request será de 10 minutos y el tiempo de duración del token de refresh será de 25 minutos.

Los endpoints solicitarán el header HTTP `Authorization` y en el campo estará `Bearer` [JWT]

```http
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zb2Z0d2FyZTEudGVzdFwvYXBpXC9sb2dpbiIsImlhdCI6MTU1ODEwNTI4OSwiZXhwIjoxNTU4MTA4ODg5LCJuYmYiOjE1NTgxMDUyODksImp0aSI6Im93ZkVWcHVjd3dpY1dBZjAiLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.BtrPc6Q4S-ylstfdhXDchzCEsuwcha6OuJWZ6vllMX4
```

Si la solicitud falla se generará el siguiente error:

```json
{
    "Message": "Token Invalid",
    "Code": 400
}
```

Si se manda un token expirado se devolverá:

```json
{
    "Message": "Token Expired",
    "Code": 307
}
```
# Group Persons
Listar y registrar personas

## Listado de People [/Person]

### Get People [GET]
Recuperar el listado de todas las personas registradas

+ Parameters

    + id_person (required, bigInt, `154`) ... La llave primaria de la persona de tipo big integer, tiene una lógica autoincremental.
    + name (required, string, `John`) ... El nombre de la persona.
    + last_name (required, string, `Smith`) ... El apellido de la persona.
    + b_day (required, date, `1997-12-10` ) ... Fecha de nacimiento de la persona.
    + address (required, string, `Star Street, Illinois Avenue`) ... Dirección domiciliaria de la persona.
    + gender (required, string, `Male`) ... El género de la persona.
    + phone (required, int, `78936475`) ... Teléfono (domicilio o móvil) de la persona.
    + country (required, string, `España`) ... País donde se encuentra de la persona.
    + city (required, string, `Saragoza`) ... Ciudad donde se encuentra la persona.
    + e-mail (required, string, `john@smith.com`) ... Correo electrónico de la persona.
    + id_img (required, integer, `1`) ... El id de la imagen de perfil que tiene la persona (Para su visualización).
    + ID_NIT (required, integer, `2334408`) ... Número de Cédula de Idenditad o Nit de la empresa.

+ Response 200 (application/json)

        {
            "data": [
                {
                    "id_person": 154,
                    "name": "John",
                    "last_name": "Smith",
                    "b_day": "1997-12-10",
                    "phone": "78945123",
                    "gender": "Male",
                    "country": "España",
                    "city": "Saragoza",
                    "e-mail": "john@smith.com",
                    "id_img": 152,
                    "ID-NIT": 2334408 
                },
                {
                    "id_person": 12352,
                    "name": "Marianne",
                    "last_name": "Carow",
                    "b_day": "1997-06-10",
                    "phone": "89745633",
                    "gender": "Female",
                    "country": "Canadá",
                    "city": "Ottawa",
                    "e-mail": "mariane@carow.com",
                    "id_img": 110,
                    "ID-NIT": 4916388
                }
            ]
        }

+ Response 404 (application/json)

        {
          "Message": "No People Registered",
          "error_code": 404
        }

## Creación de persona [/Person]

### Post Person [POST]

+ Request (application/json)

      + Body

            {
              "name": "John",
              "last_name": "Smith",
              "b_day": "1997-12-10",
              "address": "Hello St, Hi Avenue",
              "phone": "78945123",
              "gender": "Male",
              "country": "España",
              "city": "Saragoza",
              "e-mail": "john@smith.com",
              "id_img": 152,
              "ID_NIT": 2334408         
            }
+ Response 201 (application/json)

        {
            "Message": "Person added",
            "code": 201
        }

+ Response 400 (application/json)

        {
            "Message": "Person not added",
            "code": 400
        }

+ Response 400 (application/json)

        {
            "Message": "Invalid Data",
            "code": 400
        }

## Búsqueda persona específica [/Person/{id_person}]

+ Parameters

    + id_person (required, integer, `154` ) ... La llave primaria de la persona, es un big int, y de lógica autoincremental

### Get Person [GET]

+ Response 200 (application/json)

        {
            "data": [
                {
                    "id_person": 154,
                    "name": "John",
                    "last_name": "Smith",
                    "b_day": "1997-12-10",
                    "phone": "78945123",
                    "gender": "Male",
                    "country": "España",
                    "city": "Saragoza",
                    "e-mail": "john@smith.com",
                    "id_img": 152,
                    "ID_NIT": 2334408 
                }
            ]
        }

+ Response 404 (application/json)

        {
          "Message": "Person not found",
          "error_code": 404
        }

## Eliminación Person específica [/Person/{id_person}]

Se eliminará a la persona junto con su usuario registrado

+ Parameters

    + id_person (required, bigInt, `154`) ... La llave primaria de la persona de tipo big integer, tiene una lógica autoincremental.

### Delete Person [DELETE]

+ Request 

    + Headers

            Authorization: Bearer [access token]
+ Response 200 (application/json)
        {
          "Message": "Delete Successful",
          "code": 200
        }

+ Response 400 (application/json)

        {
          "Message": "Delete Failed",
          "error_code": 400
        }


## Update de datos Person [/Person/{id_person}]

Se eliminará a la persona junto con su usuario registrado

+ Parameters

    + id_person (required, bigInt, `154`) ... La llave primaria de la persona de tipo big integer, tiene una lógica autoincremental.
    + name (required, string, `John`) ... El nombre de la persona.
    + last_name (required, string, `Smith`) ... El apellido de la persona.
    + b_day (required, date, `1997-12-10` ) ... Fecha de nacimiento de la persona.
    + address (required, string, `Star Street, Illinois Avenue`) ... Dirección domiciliaria de la persona.
    + gender (required, string, `Male`) ... El género de la persona.
    + phone (required, int, `78936475`) ... Teléfono (domicilio o móvil) de la persona.
    + country (required, string, `España`) ... País donde se encuentra de la persona.
    + city (required, string, `Saragoza`) ... Ciudad donde se encuentra la persona.
    + e-mail (required, string, `john@smith.com`) ... Correo electrónico de la persona.
    + id_img (required, integer, `1`) ... El id de la imagen de perfil que tiene la persona (Para su visualización).
    + ID_NIT (required, integer, `2334408`) ... Número de Cédula de Idenditad o Nit de la empresa.

### Put Person [PUT]

+ Request (application/json)

    + Headers

            Authorization: Bearer [access token]

    + Body

            {
              "name": "John",
              "last_name": "Smith",
              "b_day": "1997-12-10",
              "address": "Hello St, Hi Avenue",
              "phone": "78945123",
              "gender": "Male",
              "country": "España",
              "city": "Saragoza",
              "e-mail": "john@smith.com",
              "id_img": 152,
              "ID_NIT": 2334408         
            }
            
+ Response 200 (application/json)
        {
          "Message": "Person edited",
          "code": 200
        }

+ Response 404 (application/json)

        {
          "Message": "Person not edited",
          "error_code": 400
        }
# Group Users
Buscar y administrar usuarios.

## Listado de Usuarios [/Person/{id_person}/User]

### Get Users [GET]
Recuperar el listado de los usuarios registrados a nombre de una persona.

+ Parameters

    + id_person (optional, string, `1`) ... El id de persona la cual sólo debe tener registado un usuario a su nombre, por ende la respuesta tiene que ser un sólo objeto.

+ Response 200 (application/json)

        {
          "data": {
              "id_user": 1,
              "username": "j.smith",
              "email": "john@smith.com",
              "email_verified_at": null,
              "salt": "software1",
              "status": "active",
              "id_person": 1
          }
        }
+ Response 404 (application/json)

        {
          "Message": "User not found",
          "error_code": 404
        }

## Registro de User [/api/register]

### Post User [POST]

Registro de User

+ Parameters

    + username (required, bigInt, `154`) ... El username para la persona.
    + email (required, string, `John`) ... El email con el cual estará registrado el usuario.
    + password (required, string, `Smith`) ... La contraseña del usuario.
    + password_confirmation (required, date, `1997-12-10` ) ... Campo de confirmación de la contraseña.
    + id_person (required, string, `Star Street, Illinois Avenue`) ... El identificador de la persona para la cual el usuario está siendo registrado.

+ Request (application/json)

      + Body
            {
              "username": Rain Man,
              "email": "rain@man.com",
              "password": "secret",
              "password_confirmation": "secret",
              "id_person": "1",    
            }

+ Response 201 (application/json)

        {
            "user": {
                    "username": "Rain Man",
                    "email": "rain@man.com",
                    "salt": "software1",
                    "status": "active",
                    "id_person": "1",
                    "id_user": 3
                      },
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zb2Z0d2FyZTEudGVzdFwvYXBpXC9yZWdpc3RlciIsImlhdCI6MTU1ODA1MzcyNywiZXhwIjoxNTU4MDU3MzI3LCJuYmYiOjE1NTgwNTM3MjcsImp0aSI6IktWSE1Nc1ozRFBocUNQY0wiLCJzdWIiOjMsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.8YqSvFnhF1ry15XvGRdbDCXhUlMcIO1fasN9XsmDaYE"
        }

# Group Enterprises

Administrar empresas 


## Listado de Enterprises [/enterprises]


### Get enterprises [GET]

Recuperar el listado de las empresas registradas.

+ Parameters
    + id_enterprise (required, biginteger,`3`) ... Este es el identificador del regitro de una empresa
    + name (required, string, `Renation.srl`) ... El nombre de la empresa registrada
    + func_lic (required, biginteger, `456578576`) ... El número de la licencia de funcionamiento de la empresa (Es único para cada empresa )
    + score (required, float, `0`) ... La puntuación que le será dado por los turistas
    + id_user (required, bigincrement, `1`) ... La llave foranea del usuario dueño (Es único y solo puede registrar una empresa a su nombre)
    
+ Response 200 (application/json)

        {
            "data": [
         {
            "id_enterprise": 3,
            "name": "Renation.srl",
            "func_lic": 456578576,
            "score": "0",
            "id_user": 1
        },
        {
            "id_enterprise": 4,
            "name": "Renation.srl",
            "func_lic": 6578576,
            "score": "0",
            "id_user": 2
        }
        ]
        }
+ Response 404 (application/json)

        {
          "Message": "Person not found",
          "error_code": 404
        }

## Registro de enterprise [/enterprises]

### Post enterprise [POST]

Registro de Enterprise

+ Request (application/json)

      + Body
        {   
             "name":"Renation.srl",
	         "func_lic":6578576,
	         "id_user":2 
        }
+ Response 201 (application/json)

        {
            "Message": "Enterprise Added",
            "code": 201,
            "data": {
                        "name": "Renation.srl",
                        "func_lic": 6578576,
                        "id_user": 2,
                        "score": 0,
                        "id_enterprise": 4
                    }
           }
+ Response 400 (application/json)

        {
          "{\"id_user\":[\"The id user has already been taken.\"]}"
        }
+ Response 400 (application/json)
        {
          "{\"func_lic\":[\"The func lic has already been taken.\"],\"id_user\":[\"The id user has already been taken.\"]}"

        }
+ Response 404 (application/json)
        {
            "Message": "User not found",
            "code": 404
        }
## Búsqueda de una empresa específica [/enterprises/{id_enterprise}]

+ Parameters

    + id_enterprise (required, integer, `3` ) ... El identificador de la empresa

### Show Enterprise [GET]

+ Response 200 (application/json)

        {
            "data": {
                        "id_enterprise": 6,
                        "name": "J.Garcia",
                        "func_lic": 159874,
                        "score": "0",
                        "id_user": 3
                    }
        }

+ Response 404 (application/json)

                    {
                    "Message": "Enterprise Not Found",
                    "error_code": 404
                    }

## Eliminación Enterprise específica [/enterprises/{id_enterprise}]

Se eliminará a la empresa 
+ Parameters

    + id_enterprise (required, integer, `6` ) ... El identificador único de la empresa

### Delete Enterprise [DELETE]

+ Request

    + Headers

            Authorization: Bearer {access token}

+ Response 200
        {
          "Message": "Enterprise deleted successfully",
          "code": 200
        }

+ Response 404 (application/json)

        {
          "Message": "Enterprise not found",
          "error_code": 404
        }

+ Response 404 (application/json)

        {
          "status": "Authorization Token not found"
        }

+ Response 401 (application/json)

        {
          "status": "Token is Invalid"
        }
--------------------------# Group Enterprise Subscriptions

Administrar las suscripciones de empresas 


## Listado de Enterprise Subscriptions [/enterprise_subscriptions]


### Get enterprise_subscriptions [GET]

Recuperar el listado de las empresas suscritas.

+ Parameters
    + id_enterprise_subscription (required, biginteger,`3`) ... Este es el identificador del regitro de una empresa
    + name (required, string, `Renation.srl`) ... El nombre de la empresa registrada
    + func_lic (required, biginteger, `456578576`) ... El número de la licencia de funcionamiento de la empresa (Es único para cada empresa )
    + score (required, float, `0`) ... La puntuación que le será dado por los turistas
    + id_user (required, bigincrement, `1`) ... La llave foranea del usuario dueño (Es único y solo puede registrar una empresa a su nombre)
    
+ Response 200 (application/json)

        {
            "data": [
         {
            "id_enterprise": 3,
            "name": "Renation.srl",
            "func_lic": 456578576,
            "score": "0",
            "id_user": 1
        },
        {
            "id_enterprise": 4,
            "name": "Renation.srl",
            "func_lic": 6578576,
            "score": "0",
            "id_user": 2
        }
        ]
        }
+ Response 404 (application/json)

        {
          "Message": "Person not found",
          "error_code": 404
        }

## Registro deenterprise_subscriptions [/enterprises]

### Post enterprise_subscriptions [POST]

Registro de Enterprise

+ Request (application/json)

      + Body
        {   
             "name":"Renation.srl",
	         "func_lic":6578576,
	         "id_user":2 
        }
+ Response 201 (application/json)

        {
            "Message": "Enterprise Added",
            "code": 201,
            "data": {
                        "name": "Renation.srl",
                        "func_lic": 6578576,
                        "id_user": 2,
                        "score": 0,
                        "id_enterprise": 4
                    }
           }
+ Response 400 (application/json)

        {
          "{\"id_user\":[\"The id user has already been taken.\"]}"
        }
+ Response 400 (application/json)
        {
          "{\"func_lic\":[\"The func lic has already been taken.\"],\"id_user\":[\"The id user has already been taken.\"]}"

        }
+ Response 404 (application/json)
        {
            "Message": "User not found",
            "code": 404
        }
## Búsqueda de una suscripción específica [/enterprises/{id_enterprise}]

+ Parameters

    + id_enterprise (required, integer, `3` ) ... El identificador de la empresa

### Show Enterprise [GET]

+ Response 200 (application/json)

        {
            "data": {
                        "id_enterprise": 6,
                        "name": "J.Garcia",
                        "func_lic": 159874,
                        "score": "0",
                        "id_user": 3
                    }
        }

+ Response 404 (application/json)

                    {
                    "Message": "Enterprise Not Found",
                    "error_code": 404
                    }

## Eliminación Enterprise específica [/enterprises/{id_enterprise}]

Se eliminará a la empresa 
+ Parameters

    + id_enterprise (required, integer, `6` ) ... El identificador único de la empresa

### Delete Enterprise [DELETE]

+ Request

    + Headers

            Authorization: Bearer {access token}

+ Response 200
        {
          "Message": "Enterprise deleted successfully",
          "code": 200
        }

+ Response 404 (application/json)

        {
          "Message": "Enterprise not found",
          "error_code": 404
        }

+ Response 404 (application/json)

        {
          "status": "Authorization Token not found"
        }

+ Response 401 (application/json)

        {
          "status": "Token is Invalid"
        }


# Group Enterprise Subscriptions
Administrar suscripciones

## Listado de enterprises [/enterprise_subscriptions]

### GET enterprises [GET]

Recuperar el listado de las empresas registradas.
+ Parameters

    + state (optional, integer, `1`) ... El estado de la suscripcion 1 activo 0 inactivo
    + price (optional, decimal, `200`) ... El costo de la suscripción
    + id_enterprise (optional, integer, `1`) ... La llave foranea de la empresa 
    + id_enterprise_subscription(optional,integer,`1`) 
    
+ Response 200 (application/json)

        {
            "data": [
                {
                    "state": 1,
                    "price": 200,
                    "id_enterprise": "1",
                    "updated_at": "2019-05-17 22:18:16",
                    "created_at": "2019-05-17 22:18:16",
                    "id_enterprise_subscription": 1
                },
                
            ]
        }
## Registro de enterprise {/enterprises} 

### POST enterprise [POST]
Registro de Enterprise

+ Request (application/json)

      + Body

            {
              "state": 1,
              "price": 200,
              "id_enterprise": "1",
              "updated_at": "2019-05-17 22:18:16",
              "created_at": "2019-05-17 22:18:16",
              "id_enterprise_subscription": 4
               
            }
+ Response 201 (application/json)

        {
           "enterprise_subscription": {
                                        "state": 1,
                                        "price": 200,
                                        "id_enterprise": "1",
                                        "updated_at": "2019-05-17 22:18:16",
                                        "created_at": "2019-05-17 22:18:16",
                                        "id_enterprise_subscription": 4
    }
           }

#Group Payments
Listado y registro de pagos de la empresa
## Listado de enterprises[/enterprise_subscriptions{?name}{&func_lic}{id_user}]
### Get enterprises [Get]
Recuperar el listado de las empresas registradas.
+ Parameters

    + state (optional, integer, `1`) ... El estado de la suscripcion 1 activo 0 inactivo
    + price (optional, decimal, `200`) ... El costo de la suscripción
    + id_enterprise (optional, integer, `1`) ... La llave foranea de la empresa 
    + id_enterprise_subscription(optional,integer,`1`) 
    
+ Response 200 (application/json)

        {
           Registrar y listar suscripciones
## Listado de enterprises[/enterprise_subscriptions{?id_payment}{&name}{$last_name}{$card_number}{$expiration_date}{$security_code}{$pay_day}{$next_payment_day}{$description}{$payment_format}{$total}{$state}{$id_enterprise_subscription}]
### Get enterprises [Get]
Recuperar el listado de las empresas registradas.
+ Parameters

    + id_payment (optional, integer, `14`) ... id del pago
    + name (optional, string, `ernesto`) ...Nombre del propietario de la tarjeta de credito
    + last_name(optional, string, `clarke`),,,Apellido del propietario de la tarjeta de credito
    + card number (optional, integer, `5468798`) ... El numero de la tarjeta de credito
    + id_enterprise_subscription(optional,integer,`1`) 
    
+ Response 200 (application/json)

        {
            "data_all": [
                        {
                          "id_payment": 14,
                          "name": "ernesto",
                          "last_name": "clarke",
                          "card_number": 5468798,
                          "expiration_date": 2020,
                          "security_code": 566,
                          "pay_day": "2019-05-17",
                          "next_payment_day": "2019-06-16",
                          "description": "pago de suscripcion",
                          "service_period": "29",
                          "payment_format": "visa",
                          "total": "200",
                          "state": 1,
                          "id_enterprise_subscription": 1,
                          "deleted_at": null,
                          "created_at": "2019-05-17 21:40:37",
                          "updated_at": "2019-05-17 21:40:37"
                           },
                      {
                          "id_payment": 15,
                          "name": "ernesto",
                          "last_name": "clarke",
                          "card_number": 5468798,
                          "expiration_date": 2020,
                          "security_code": 566,
                          "pay_day": "2019-05-17",
                          "next_payment_day": "2019-06-16",
                          "description": "pago de suscripcion",
                          "service_period": "29",
                          "payment_format": "visa",
                          "total": "200",
                          "state": 1,
                          "id_enterprise_subscription": 1,
                          "deleted_at": null,
                          "created_at": "2019-05-17 21:43:55",
                          "updated_at": "2019-05-17 21:43:55"
                      },
                     
                     
        }
## Registro de pagos {/Payments} 
### POST Payments [POST]
Registro de pago

+ Request (application/json)

      + Body

            {
              
                        "total": 200,
                        "state": 1,
                        "description": "pago de suscripcion",
                        "name": "ernesto",
                        "last_name": "clarke",
                        "card_number": "5468798",
                        "expiration_date": "2020",
                        "pay_day": "2019-05-17T21:49:00.285805Z",
                        "next_payment_day": "2019-06-16",
                        "security_code": "566",
                        "service_period": "29",
                        "payment_format": "visa",
                        "id_enterprise_subscription": "2",
                        "updated_at": "2019-05-17 21:49:00",
                        "created_at": "2019-05-17 21:49:00",
                        "id_payment": 18
            }
               
            }
+ Response 201 (application/json)

        {
          "Message": "Pago realizado con exito!",
                              "payment": {
                              "total": 200,
                              "state": 1,
                              "description": "pago de suscripcion",
                              "name": "ernesto",
                              "last_name": "clarke",
                              "card_number": "5468798",
                              "expiration_date": "2020",
                              "pay_day": "2019-05-17T21:49:00.285805Z",
                              "next_payment_day": "2019-06-16",
                              "security_code": "566",
                              "service_period": "29",
                              "payment_format": "visa",
                              "id_enterprise_subscription": "2",
                              "updated_at": "2019-05-17 21:49:00",
                              "created_at": "2019-05-17 21:49:00",
                              "id_payment": 18
                          }
    }
           }Registrar y listar suscripciones
## Listado de enterprises[/enterprise_subscriptions{?name}{&func_lic}{id_user}]
### Get enterprises [Get]
Recuperar el listado de las empresas registradas.
+ Parameters

    + state (optional, integer, `1`) ... El estado de la suscripcion 1 activo 0 inactivo
    + price (optional, decimal, `200`) ... El costo de la suscripción
    + id_enterprise (optional, integer, `1`) ... La llave foranea de la empresa 
    + id_enterprise_subscription(optional,integer,`1`) 
    
+ Response 200 (application/json)

        {
            "data": [
                {
                    "state": 1,
                    "price": 200,
                    "id_enterprise": "1",
                    "updated_at": "2019-05-17 22:18:16",
                    "created_at": "2019-05-17 22:18:16",
                    "id_enterprise_subscription": 1
                },
                
            ]
        }
## Registro de enterprise {/enterprises} 
### POST enterprise [POST]
Registro de Enterprise

+ Request (application/json)

      + Body

            {
              "state": 1,
              "price": 200,
              "id_enterprise": "1",
              "updated_at": "2019-05-17 22:18:16",
              "created_at": "2019-05-17 22:18:16",
              "id_enterprise_subscription": 4
               
            }
+ Response 201 (application/json)

        {
           "enterprise_subscription": {
                                        "state": 1,
                                        "price": 200,
                                        "id_enterprise": "1",
                                        "updated_at": "2019-05-17 22:18:16",
                                        "created_at": "2019-05-17 22:18:16",
                                        "id_enterprise_subscription": 4
    }
           }
        }
## Registro de enterprise {/enterprises} 
### POST enterprise [POST]
Registro de Enterprise

+ Request (application/json)

      + Body

            {
              "state": 1,
              "price": 200,
              "id_enterprise": "1",
              "updated_at": "2019-05-17 22:18:16",
              "created_at": "2019-05-17 22:18:16",
              "id_enterprise_subscription": 4
               
            }
+ Response 201 (application/json)

        {
           "enterprise_subscription": {
                                        "state": 1,
                                        "price": 200,
                                        "id_enterprise": "1",
                                        "updated_at": "2019-05-17 22:18:16",
                                        "created_at": "2019-05-17 22:18:16",
                                        "id_enterprise_subscription": 4
    }
           }

+ Response 404 (application/json)

        "{\"password\":[\"The password confirmation does not match.\"]}"

+ Response 404 (application/json)

        "{\"password\":[\"The password confirmation does not match.\"],\"id_person\":[\"The id person has already been taken.\"]}"

## Login de Usuario [/api/login]

### Login User [POST]

Ingreso de credenciales para ser verificadas y recibir un token JWT

+ Request (application/json)

      + Body
            {
              "email": "kayle@mesa.com",
              "password": "secret"
            }

+ Response 200 (application/json)

        {
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zb2Z0d2FyZTEudGVzdFwvYXBpXC9sb2dpbiIsImlhdCI6MTU1ODEyOTU2NCwiZXhwIjoxNTU4MTMzMTY0LCJuYmYiOjE1NTgxMjk1NjQsImp0aSI6Im1xaEZnWXk1ck9PYVdoQnoiLCJzdWIiOjQsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.dnwnvT4Syz31YDg1p0ltrHklb8EklIqwbKqWV_HMJw8"
        }         
+ Response 400 (application/json)

        {
            "error": "invalid_credentials"
        }

# Group Verificators
Buscar y administrar verificadores

## Verificators List [/places{?lat}{&lon}{&distance}{&box}{&number}{&page}]

### Get Verificators [GET]
Locate places close to a certain set of coordinates, or provide a box of coordinates to search within.

+ Parameters

    + lat (optional, number, `40.7641`) ... Latitude to search near, with any accuracy
    + lon (optional, number, `-73.9866`) ... Longitude to search near, with any accuracy
    + distance = `10` (optional, number, `20`) ... The radius size in miles to search for from lat and lon coordinates
    + box (optional, string, `40.7641,-73.9866,40.7243,-73.9841`) ... Top left latitude, top left longitude, bottom right latitude, bottom right longitude
    + number (optional, integer, `15`) ... The number of results to return per page
    + page = `1` (optional, integer, `15`) ... Which page of the result data to return


+ Response 200 (application/json)

        {
            "data": [
                {
                    "id": 2,
                    "name": "Videology",
                    "lat": 40.713857,
                    "lon": -73.961936,
                    "created_at": "2013-04-02"
                },
                {
                    "id": 1,
                    "name": "Barcade",
                    "lat": 40.712017,
                    "lon": -73.950995,
                    "created_at": "2012-09-23"
                }
            ]
        }

## Create Verificator [/places]

### Post de un Verificador [POST]

+ Request (application/json)

      + Body

            {
                "name": "Videology",
                "lat": 40.713857,
                "lon": -73.961936
            }

+ Response 201 (application/json)

        {
            "data": [
                "id": 2,
                "name": "Videology",
                "lat": 40.713857,
                "lon": -73.961936,
                "created_at": "2013-04-02"
            ]
        }



## Verificador [/places/{id}]
Manage an existing place.

+ Parameters

    + id (required, integer) ... The unique identifier of a place

### Get place [GET]

+ Response 200
+ Response 404 (application/json)

        {
          "error" : {
            "code" : "GEN-LIKETHEWIND",
            "http_code" : 404,
            "message" : "Resource Not Found"
          }
        }

### Modify place [PUT]

+ Request

    + Headers

            Authorization: Bearer {access token}

+ Response 200

### Delete place [DELETE]

+ Request

    + Headers

            Authorization: Bearer {access token}

+ Response 200
+ Response 404 (application/json)

        {
          "error" : {
            "code" : "GEN-LIKETHEWIND",
            "http_code" : 404,
            "message" : "Resource Not Found"
          }
        }

# Group verifications
Buscar y administrar verificadores

## verifications List [/places{?lat}{&lon}{&distance}{&box}{&number}{&page}]

### Get verifications [GET]
Locate places close to a certain set of coordinates, or provide a box of coordinates to search within.

+ Parameters

    + lat (optional, number, `40.7641`) ... Latitude to search near, with any accuracy
    + lon (optional, number, `-73.9866`) ... Longitude to search near, with any accuracy
    + distance = `10` (optional, number, `20`) ... The radius size in miles to search for from lat and lon coordinates
    + box (optional, string, `40.7641,-73.9866,40.7243,-73.9841`) ... Top left latitude, top left longitude, bottom right latitude, bottom right longitude
    + number (optional, integer, `15`) ... The number of results to return per page
    + page = `1` (optional, integer, `15`) ... Which page of the result data to return


+ Response 200 (application/json)

        {
            "data": [
                {
                    "id": 2,
                    "name": "Videology",
                    "lat": 40.713857,
                    "lon": -73.961936,
                    "created_at": "2013-04-02"
                },
                {
                    "id": 1,
                    "name": "Barcade",
                    "lat": 40.712017,
                    "lon": -73.950995,
                    "created_at": "2012-09-23"
                }
            ]
        }

## Create verification [/places]

### Post de una verification [POST]

+ Request (application/json)

      + Body

            {
                "name": "Videology",
                "lat": 40.713857,
                "lon": -73.961936
            }

+ Response 201 (application/json)

        {
            "data": [
                "id": 2,
                "name": "Videology",
                "lat": 40.713857,
                "lon": -73.961936,
                "created_at": "2013-04-02"
            ]
        }



## Verification [/places/{id}]
Manage an existing place.

+ Parameters

    + id (required, integer) ... The unique identifier of a place

### Get verification [GET]

+ Response 200
+ Response 404 (application/json)

        {
          "error" : {
            "code" : "GEN-LIKETHEWIND",
            "http_code" : 404,
            "message" : "Resource Not Found"
          }
        }

### Modify verification [PUT]

+ Request

    + Headers

            Authorization: Bearer {access token}

+ Response 200

### Delete verification [DELETE]

+ Request

    + Headers

            Authorization: Bearer {access token}

+ Response 200
+ Response 404 (application/json)

        {
          "error" : {
            "code" : "GEN-LIKETHEWIND",
            "http_code" : 404,
            "message" : "Resource Not Found"
          }
        }
# Group Transport
## Listado de Transport [/Tranport]
### Get Tranport [GET]
Indexar todos los registros de Transport registrados

+ Parameters
    + id_tranport (required, bigInt, `1`) ... La llave primaria de Transport de tipo big integer, tiene una lógica autoincremental.
    + id_transport_type (required, bigInt, `1`) ... El tipo de Transporte.
    + id_transport_enterprise (required, bigInt, `1`) ... La empresa para el Transporte.
    + deleted_at (required, timestamp, `null` ) ... Fecha de Eliminacion de registro.
    + created_at (required, timestamp, `2019-05-05 18:59:10`) ... Fecha de Creacion de registro.
    + updated_at (required, timestamp, `2019-05-05 18:59:15`) ... Fecha de Actualizacion de registro.

+ Response 200 (application/json)
{
    "data": [
        {
            "id_transport": 1,
            "id_transport_type": 1,
            "id_transport_enterprise": 1,
            "deleted_at": null,
            "created_at": "2019-05-05 18:59:10",
            "updated_at": "2019-05-05 18:59:15"
        }
    ]
}

+ Response 404 (application/json)

        {
          "Message": "No Transports Registered",
          "error_code": 404
        }

## Creación de Transporte [/Tranport]

### Post Tranport [POST]

+ Request (application/json)

      + Body

        {
	          "id_transport_type":1,
	          "id_transport_enterprise":1
        }
+ Response 201 (application/json)

        {
            "Message": "Transport Added",
            "code": 201
        }
+ Response 400 (application/json)
        {
            "0": "code",
            "1": 400,
            "Message": "Bad Input"
        }

## Búsqueda de Transporte específico [/Tranport/{id_tranport}]

+ Parameters

    + id_tranport (required, integer, `1` ) ... El identificador único del transporte

### Get Transport [GET]

+ Response 200 (application/json)

        {
    "data": [
        {
            "id_transport": 1,
            "id_transport_type": 1,
            "id_transport_enterprise": 1,
            "deleted_at": null,
            "created_at": "2019-05-05 18:59:10",
            "updated_at": "2019-05-05 18:59:15"
            }
          ]
        }


+ Response 404 (application/json)

        {
          "Message": "Transport not found",
          "error_code": 404
        }