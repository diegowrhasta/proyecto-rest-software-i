@extends('layouts.app')
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Crowdlendig</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/blog/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script
    src="https://code.jquery.com/jquery-3.4.0.min.js"
    integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
    crossorigin="anonymous"></script>
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/css/style.css" rel="stylesheet">

    

  </head>

@section('content')
<div class="container">
<a class="navbar-brand" href="{{ url('/home') }}">Menu principal</a>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Bienvenido</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('guardarempresa') }}">
           
                @csrf
                    
                
                    <div class="row">
                        <div class=" col-md-7 form-group">
                            <label  style="font-size:15px; font-style: bold; color: #243366;">Nombre Empresa:</label>
                            <input type="text"  class="form-control" name= "montosol" id="montosol"  value="{{ old('montosol') }}"  onkeypress="return soloLetras(event)" maxlength="9" required>
                           
                        </div> 
                      
                    </div>                               
                    
                    
                  <div class="row">

                    <div class="col-md-7 form-group" role="group" aria-label="...">
                            <label style="font-size:15px; font-style: bold; color: #243366;" for=" ">NIT: </label>
                            <input type="text" class="form-control" name= "plazo" id="plazo" value="{{ old('plazo') }}" placeholder="" onkeypress="return soloLetras2(event)"  maxlength="2" required>
                          
                    </div>
                    
                  </div>
                  
                   
                        <input type="submit" value="Continuar" class="btn btn-primary" >
                        
                </form>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


<script>
    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " abcdefghijklmnñopqrstuvwxyz-_ABCDEFGHIJKLMNÑOPQRSTUVWXYZ.,'áéíóú";
        especiales = [8, 37, 39, 46];
    
        tecla_especial = false
        for(var i in especiales) {
            if(key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
    
        if(letras.indexOf(tecla) == -1 && !tecla_especial)
            return false;
    }
    
    
</script>

<script>
function soloLetras2(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "0123456789";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}


</script>