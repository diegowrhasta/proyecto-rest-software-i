<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware' => ['jwt.verify']], function() {
    Route::resource('Verificator','VerificatorController');
    Route::resource('Verification','VerificationController');
    Route::delete('Person/{id}', 'PersonController@destroy');

    Route::post('enterprises','EnterpriseController@create');
    Route::get('enterprises/{id_enterprise}','EnterpriseController@show');
    Route::get('enterprises','EnterpriseController@list');
    Route::put('enterprises/{id_enterprise}','EnterpriseController@update');
    Route::delete('enterprises/{id_enterprise}','EnterpriseController@delete'); 

    Route::post('enterprise_subscriptions','SubscriptionsController@create');
    Route::get('enterprise_subscriptions/{id_enterprise_subscription}','SubscriptionsController@show');
    Route::get('enterprise_subscriptions/{id_enterprise_subscription}','SubscriptionsController@list');
    Route::get('enterprise_subscriptions','SubscriptionsController@all');
    Route::put('enterprise_subscriptions/{id_enterprise}','SubscriptionsController@update');
    Route::delete('enterprise_subscriptions/{id_enterprise}','SubscriptionsController@delete'); 

    Route::post('payments','PaymentsController@create');
    Route::get('payments/{id_payment}','PaymentsController@show');
    Route::get('payments/{id_payment}','PaymentsController@list');
    Route::get('payments','PaymentsController@all');
    Route::put('payments/{id_payment}','PaymentsController@update');
    Route::delete('payments/{id_payment}','PaymentsController@delete'); 
    Route::put('Person/{id_person}', 'PersonController@update');
    Route::get('Person/{id_person}/User', 'UserController@index');
    Route::get('Person/{id_person}/User/{id_user}', 'UserController@show');
    Route::put('Person/{id_person}/User/{id_user}', 'UserController@update');
    Route::delete('Person/{id_person}/User/{id_user}', 'UserController@destroy');


});
Route::resource('Test', 'TestingController');
Route::resource('Person','PersonController');
//Route::resource('Person.User', 'UserController');
Route::group(['middleware' => ['jwt.verify']], function () {
    Route::resource('Tranport', 'TranportController');
});

Route::group(['middleware' => ['jwt.verify']], function () {
Route::resource('Package','Package\PackageController');
Route::group(['prefix'=>'Package'],function(){
    Route::resource('/{Package}/Activity','ActivityController');
});
});

Route::group(['middleware' => ['jwt.verify']], function () {
Route::resource('Package_sale','Package_saleController');
Route::resource('Claim','ClaimController',['only'=>['index','show']]);
Route::resource('Package_sale.Claim','Package_saleClaimController',['except'=>['show']]);
});
