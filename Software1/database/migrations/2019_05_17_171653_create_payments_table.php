<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Foundation\Testing\Constraints\SoftDeletedInDatabase;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id_payment');
            $table->string('name');
            $table->string('last_name');
            $table->integer('card_number');
            $table->integer('expiration_date');
            $table->integer('security_code');
            $table->date('pay_day');
            $table->date('next_payment_day');
            $table->string('description');
            $table->string('service_period');
            $table->string('payment_format');
            $table->string('total');
            $table->integer('state');
            $table->bigInteger('id_enterprise_subscription')->unsigned();
            $table->foreign('id_enterprise_subscription')->references('id_enterprise_subscription')->on('enterprise_subscriptions');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
