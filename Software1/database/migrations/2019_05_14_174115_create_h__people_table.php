<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('h__people', function (Blueprint $table) {
            $table->bigIncrements('id_h_people');
            $table->bigInteger('id_person');
            $table->string('name');
            $table->string('last_name');
            $table->date('b_day');
            $table->string('address');
            $table->integer('phone');
            $table->string('gender');
            $table->string('country');
            $table->string('city');
            $table->string('e-mail');
            $table->string('id_img');
            $table->integer('ID-NIT');
            $table->timestamp('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('h__people');
    }
}
