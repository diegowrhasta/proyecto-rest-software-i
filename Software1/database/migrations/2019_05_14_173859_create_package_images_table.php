<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_images', function (Blueprint $table) {
            $table->bigIncrements('id_package_image');
            $table->bigInteger('id_package')->unsigned();
            $table->foreign('id_package')->references('id_package')->on('packages');
            $table->bigInteger('id_img')->unsigned();
            $table->foreign('id_img')->references('id_img')->on('images');
            $table->timestamp('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_images');
    }
}
