<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsTable extends Migration
{
    
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->bigIncrements('id_claim');
            $table->string('commentary');
            $table->bigInteger('package_sale_id_package_sale')->unsigned();
            $table->foreign('package_sale_id_package_sale')->references('id_package_sale')->on('package_sales');
            $table->timestamp('deleted_at');
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}
