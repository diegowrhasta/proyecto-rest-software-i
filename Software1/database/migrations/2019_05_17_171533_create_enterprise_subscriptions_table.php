<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterpriseSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id_enterprise_subscription');
            $table->integer('state');
            $table->decimal('price',5,2);
            $table->bigInteger('id_enterprise')->unsigned();
            $table->foreign('id_enterprise')->references('id_enterprise')->on('enterprises');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_subscriptions');
    }
}
