<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('h__users', function (Blueprint $table) {
            $table->bigIncrements('id_h_user');
            $table->bigInteger('id_user');
            $table->string('username');
            $table->string('salt');
            $table->string('status');
            $table->integer('id_person');
            $table->timestamp('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('h__users');
    }
}
