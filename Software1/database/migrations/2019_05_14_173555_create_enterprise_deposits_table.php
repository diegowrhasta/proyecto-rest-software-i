<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterpriseDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_deposits', function (Blueprint $table) {
            $table->bigIncrements('id_enterprise_deposit');
            $table->bigInteger('id_deposit')->unsigned();
            $table->foreign('id_deposit')->references('id_deposit')->on('deposits');
            $table->bigInteger('id_enterprise')->unsigned();
            $table->foreign('id_enterprise')->references('id_enterprise')->on('enterprises');
            $table->timestamp('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_deposits');
    }
}
