<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterprisePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_packages', function (Blueprint $table) {
            $table->bigIncrements('id_enterprise_package');
            $table->bigInteger('id_package')->unsigned();
            $table->foreign('id_package')->references('id_package')->on('packages');
            $table->bigInteger('id_enterprise')->unsigned();
            $table->foreign('id_enterprise')->references('id_enterprise')->on('enterprises');
            $table->timestamp('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_packages');
    }
}
