<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package__offers', function (Blueprint $table) {
            $table->bigIncrements('id_package_offer');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->float('price_percentage');
            $table->boolean('status');
            $table->bigInteger('id_package')->unsigned();
            $table->foreign('id_package')->references('id_package')->on('packages');
            $table->timestamp('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package__offers');
    }
}
