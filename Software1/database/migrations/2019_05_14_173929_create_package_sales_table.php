<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_sales', function (Blueprint $table) {
            $table->bigIncrements('id_package_sale');
            $table->double('raw_amount');
            $table->date('sale_date');
            $table->float('score');
            $table->string('comment');
            $table->bigInteger('id_tourist')->unsigned();
            $table->foreign('id_tourist')->references('id_tourist')->on('tourists');
            $table->bigInteger('id_package')->unsigned();
            $table->foreign('id_package')->references('id_package')->on('packages');
            $table->timestamp('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_sales');
    }
}
