<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('h__packages', function (Blueprint $table) {
            $table->bigIncrements('id_h_package');
            $table->bigInteger('id_package');
            $table->string('name');
            $table->double('price');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('max_adults');
            $table->integer('max_children');
            $table->date('creation_date');
            $table->float('score');
            $table->timestamp('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('h__packages');
    }
}
