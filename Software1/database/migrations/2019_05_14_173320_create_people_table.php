<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->bigIncrements('id_person');
            $table->string('name');
            $table->string('last_name');
            $table->date('b_day');
            $table->string('address');
            $table->integer('phone');
            $table->string('gender');
            $table->string('country');
            $table->string('city');
            $table->string('id_img');
            $table->integer('ID_NIT');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
