<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tranports', function (Blueprint $table) {
            $table->bigIncrements('id_transport');
            $table->bigInteger('id_transport_type')->unsigned();
            $table->foreign('id_transport_type')->references('id_transport_type')->on('transport__types');
            $table->bigInteger('id_transport_enterprise')->unsigned();
            $table->foreign('id_transport_enterprise')->references('id_transport_enterprise')->on('tranport__enterprises');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tranports');
    }
}
