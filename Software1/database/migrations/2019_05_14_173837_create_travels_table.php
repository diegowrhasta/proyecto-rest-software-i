<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travels', function (Blueprint $table) {
            $table->bigIncrements('id_travel');
            $table->bigInteger('duration');
            $table->double('distance');
            $table->boolean('final_travel');
            $table->boolean('first_travel');
            $table->bigInteger('id_location_start')->unsigned();
            $table->foreign('id_location_start')->references('id_location')->on('locations');
            $table->bigInteger('id_location_end')->unsigned();
            $table->foreign('id_location_end')->references('id_location')->on('locations');
            $table->bigInteger('id_transport')->unsigned();
            $table->foreign('id_transport')->references('id_transport')->on('tranports');
            $table->bigInteger('id_package')->unsigned();
            $table->foreign('id_package')->references('id_package')->on('packages');
            $table->timestamp('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travels');
    }
}
