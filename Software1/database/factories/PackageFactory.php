<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */
use Faker\Generator as Faker;

$factory->define(App\Package::class, function(Faker $faker){
    
    return [
         'name'=>$faker->word,
         'price'=>$faker->numberBetween(100,1000),
         'start_date'=>$faker->dateTimeBetween($startDate = '-1 years', $endDate = '+1 years'),
         'end_date'=>$faker->dateTimeBetween($startDate = '-1 years', $endDate = '+1 years'),
         'max_adults'=>$faker->numberBetween(1,40),
         'max_children'=>$faker->numberBetween(1,20),
         'creation_date'=>$faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
         'score'=>$faker->randomDigit,
    ];
});