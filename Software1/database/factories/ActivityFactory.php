<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Package;

use Faker\Generator as Faker;

$factory->define(App\Activity::class, function(Faker $faker){
    return [
        
        'package_id_package' => function(){
            return Package::all()->random();
         },
         'name'=>$faker->name,
         'date'=>$faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
         'time_start'=>$faker->time($format = 'H:i:s', $max = 'now'),
         'time_end'=>$faker->time($format = 'H:i:s', $min = '20:49:42'),
         'description'=>$faker->word
    ];
});
