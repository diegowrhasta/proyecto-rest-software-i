<?php

namespace App\Http\Resources;

use App\Http\Resources\BaseResource;
class ActivityResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public static $map = [
        "id_activity" => "id_activity",
        "name" => "name",            
        "date" => "date",           
        "time_start" => "time_start",
        "time_end" => "time_end",
        "description" => "description",
        "package_id_package" => "package_id_package"  
    ];
    public function generateLinks($request)
    {
        return [
            [
                "rel" => "show",
                "href"=> route("Package.show",$this->package_id_package),
            ],
            [
                "rel" => "package.activity",
                "href"=> route("Activity.index",$this->package_id_package),
            ]            
            
        ];
    }
    
}
