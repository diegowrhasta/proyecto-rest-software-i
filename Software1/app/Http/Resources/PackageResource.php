<?php

namespace App\Http\Resources;

//use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
class PackageResource extends BaseResource
{
    
    public static $map = [
        "id_package" => "id_package",
        "name" => "name",            
        "price" => "price",           
        "start_date" => "start_date",
        "end_date" => "end_date",
        "max_adults" => "max_adults",
        "max_children" => "max_children",
        "creation_date" => "creation_date",
        "score" => "score" 
    ];
    
    public function generateLinks($request)
    {
        return [
            [
                "rel" => "show",
                "href"=> route("Package.show",$this->id_package),
            ],
            [
                "rel" => "package.activity",
                "href"=> route("Activity.index",$this->id_package),
            ]            
            
        ];
    }
}