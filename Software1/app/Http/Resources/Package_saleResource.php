<?php

namespace App\Http\Resources;

use App\Http\Resources\BaseResource;
class Package_saleResource extends BaseResource
{
    
    
    public static $map = [
        "id_package_sale" => "id_package_sale",
        "raw_amount" => "raw_amount",            
        "sale_date" => "sale_date",           
        "score" => "score",
        "comment" => "comment",
        "id_tourist" => "id_tourist",
        "id_package" => "id_package"
    ];
    
    public function generateLinks($request)
    {
        return [
            [
                "rel" => "Package_sale",
                "href"=> route("Package_sale.show",$this->id_package_sale),
            ]    
            
        ];
    }
}