<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

abstract class BaseResource extends Resource
{
    public static $map = [];
    
    public static function mapAttribute($attribute,$invert=FALSE)
    {
        if($invert)
            return (array_flip(static::$map)[$attribute]);
        return static::$map[$attribute];
    }
    public function toArray($request)
    {
        $visibleAttributes = $this->resource->attributesToArray();
        $arAttrMapped = [];
        
        foreach($visibleAttributes as $attribute => $value)
            $arAttrMapped[static::mapAttribute($attribute)] = $value;
        
        if(method_exists($this,"generateLinks"))
        {
            $arHateoas = [
                "links" => $this->generateLinks($request)
            ];
            
            return array_merge($arAttrMapped,$arHateoas);
        }
        
        return $arAttrMapped;
    }
}
