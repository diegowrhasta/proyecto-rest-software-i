<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Enterprise;
use App\User;
class EnterpriseController extends Controller
{
   
    public function create(Request $request){
        $id_user=$request->json('id_user');
        
        $users=User::find($id_user);
        
        if(!$users){

            return response()->json(['Message'=>'User not found','code'=>404],404);
        }
               
        $validator = Validator::make($request->json()->all(), [
          'name' => 'required|string|max:255',
          'func_lic' => 'required|numeric|unique:enterprises',
          'id_user' => 'required|numeric|unique:enterprises', 
          
    ]);
    if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
    }
    else{
        
        $enterprise = Enterprise::create([
            'name' =>  $request->json('name'),
            'func_lic' => $request->json('func_lic'),
            'id_user' => $request->json('id_user'),
            'score' =>  0,
        ]);
    }
   

    return response()->json(['Message'=>'Enterprise Added','code'=>201,'data'=>$enterprise],201);
}
   
    
    
    public function show($id_enterprise)
    {
        $enterprise=Enterprise::find($id_enterprise);
        if(!$enterprise){
            return response()->json(['Message'=>'Enterprise Not Found','error_code'=>404],404);
        }
        return response()->json(['data'=>$enterprise],200);
    }

   
    public function list()
    {
        
         $enterprise=Enterprise::all();
         
        return strlen($enterprise);
        if(strlen($enterprise)<1){
            return response()->json(['Message'=>'No Enterprise Registered','code'=>404],404);
        }
        else{
            return response()->json(['data'=>Enterprise::all()],200);
        }
      
    }
   
    public function update(Request $request, $id_enterprise)
    {
        $enterprise=Enterprise::find($id_enterprise);
        if(!$enterprise){
            return response()->json(['Message'=>'Enterprise error','code'=>404],404);
        }

        $enterprise->name = $request->json('name');
        $enterprise->func_lic = $request->json('func_lic');
        
        $enterprise->save();

        return response()->json(['data_show'=>$enterprise],202);
        
    }

    
    public function destroy($id_enterprise)
    {
        $enterprise=Enterprise::find($id_enterprise);
        if(!$enterprise){
            return response()->json(['Message'=>'Enterprise error','code'=>404],404);
        }
        
        $enterprise = Enterprise::destroy($id_enterprise);
        return response()->json(['Message'=>"Enterprise deleted successfully",'code'=>200],200);
   
              
    }
}
