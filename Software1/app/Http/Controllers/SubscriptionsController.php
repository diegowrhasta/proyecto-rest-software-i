<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Deposit;
use App\Enterprise;
use App\enterprise_deposit;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Enterprise_subscription;

class SubscriptionsController extends Controller
{   
       
     public function create(Request $request){
        $id_enterprise=$request->json('id_enterprise');
        
        $enterprise=Enterprise::find($id_enterprise);
        
        if(!$enterprise){

            return response()->json(['Message'=>'Enterprise not found','code'=>404],404);
        }
        
        $enterprise_subscription = Enterprise_subscription::create([
            'state' =>  1,
            'price' =>  200,
            'id_enterprise' =>  $request->post('id_enterprise'),            
        ]);
    
       
    return response()->json(['Message'=>'Subscription Added','code'=>201,'data'=>$enterprise_subscription],201);
}
   
    
    
    public function show($id_enterprise_subscription)
    {
        $enterprise_subscription=Enterprise_subscription::find($id_enterprise_subscription);
        if(!$enterprise_subscription){
            return response()->json(['Message'=>'Enterprise Subscription Not Found','codigo'=>404],404);
        }
        return response()->json(['data_show'=>$enterprise_subscription],200);
    }

    public function all()
    {
         $enterprise_subscription=Enterprise_subscription::all();
        if(!$enterprise_subscription){
            return response()->json(['Message'=>'Enterprise Subscription error','codigo'=>404],404);
        }
        return response()->json(['data_all'=>$enterprise_subscription],200);
    }
    
    public function list($id_enterprise)
    {
         $enterprise_subscription=Enterprise_subscription::whereRaw('id_enterprise = ?',[$id_enterprise])->get();
        if(!$enterprise_subscription){
            return response()->json(['Message'=>'Enterprise Subscription Not Found','codigo'=>404],404);
        }
        return response()->json(['data'=>$enterprise_subscription],200);
    }
   
    public function update(Request $request, $id_enterprise_subscription)
    {
        $enterprise_subscription=Enterprise_subscription::find($id_enterprise_subscription);
        if(!$enterprise_subscription){
            return response()->json(['Message'=>'Enterprise Subscription error','codigo'=>404],404);
        }

        $enterprise_subscription->state = $request->post('state');
        
        
        $enterprise_subscription->save();

        return response()->json(['data_show'=>$enterprise_subscription],200);
        
    }

    
    public function destroy($id_enterprise_subscription)
    {
        
        $enterprise_subscription=Enterprise_subscription::find($id_enterprise_subscription);
        if(!$enterprise_subscription){
            return response()->json(['Message'=>'Enterprise Subscription error','codigo'=>404],404);
        }
        $enterprise_subscription = Enterprise_subscription::destroy($id_enterprise_subscription);
        
        return response()->json(['Message'=>'Suscripción de la Empresa eliminada','code'=>200],200);
    }

      
       
}
