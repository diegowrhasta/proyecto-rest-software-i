<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Verificator;
class VerificatorController extends Controller
{
    public function index(){
        $verificator=Verificator::all();
        if(sizeof($verificator)<1){
            return response()->json(['Message'=>'No Verificators Found','error_code'=>404],404);
        }
        return response()->json(['data'=>$verificator],200);
    }
    public function store(Request $request){
        if(!$request->get('full_name')||!$request->get('username')||!$request->get('password')||!$request->get('salt') ){
            return response()->json(['Message'=>'Invalid Data','error_code'=>404],404);
        }
        else{
            Verificator::create($request->all());
            return response()->json(['Message'=>'Verificator Added'],202);
        }  
    }
}
