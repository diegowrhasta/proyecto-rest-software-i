<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use \Firebase\JWT\JWT;
use PHPUnit\Framework\Exception;
use App\Http\BL\UserBL;
use App\Http\DAO\UserDAO;
use App\Http\DAO\PersonDAO;

class UserController extends Controller
{
    public function register(Request $request){
        try{
            $validator = Validator::make($request->json()->all(), [
                'username' => 'required|string|max:255|unique:users',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'id_person' => 'required|numeric|unique:users',
                ]);
                if($validator->fails()){
                    return response()->json($validator->errors()->toJson(), 400);
                }
                else{
                $userBL = new UserBL;
                $user = $request->json()->all();
                $response=$userBL->sendUser($user);
                    if(!$response){
                        return response()->json(['Message'=>'User not added','Code'=>400],400);
                    }   
                    else{
                        return response()->json(['Message'=>'User added','Code'=>201],201);
                    } 
                }
        }
        catch(Exception $e){
            return response()->json(['Message'=>$e->getMessage()],500);
        }
    } 
    public function authenticate(Request $request){
        $credentials = $request->json()->all();
        //Bearer Token
        $userBL = new UserBL;
        $response = $userBL->checkUser($credentials);
        $json_response = json_decode($response);
        if(!$response){
            return response()->json(['Message'=>'Wrong Credentials','Code'=>404],404);
        }
        else {
            return response()->json(['bearer_token'=>$json_response->{'bearer_token'},'refresh_token'=>$json_response->{'refresh_token'}],200);
        }
    }
    public function index($id_person){
        $userDAO = new UserDAO;
        $users = $userDAO->getUserwithAttrib('id_person',$id_person);
        if(!$users){
            return response()->json(['Message'=>'No Users Registered','Code'=>404],404);
        }
        else{
            return response()->json(['data'=>$users],200);
        }
    }
    public function show($id_person,$id_user){
        $userBL = new UserBL;
        $user = $userBL->prepareShow($id_person,$id_user);
        if(!$user){
            return response()->json(['Message'=>'User not Found','Code'=>404],404);
        }
        else{
            return response()->json(['data'=>$user],200);
        }
    }
    public function update(Request $request,$id_person,$id_user){
        $validator = Validator::make($request->json()->all(), [
            'username' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
            'id_person' => 'required|numeric',
            ]);
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
            }
            else{
                $userBL = new UserBL; 
                $user_new = $request->json()->all();
                $response = $userBL->prepareUpdate($user_new,$id_user);
                if(!$response){
                    return response()->json(['Message'=>'Edit Unsuccesful','Code'=>500],500);        
                }
                else{
                    return response()->json(['Message'=>'Edit Succesful','Code'=>200],200);        
                }
            }
    }
    public function destroy($id_person,$id_user){
        //BL
        $userBL = new UserBL;
        if(!$userBL->prepareDestroy($id_person,$id_user)){
            return response()->json(['Message'=>"Delete Failed",'error_code'=>500],500);
        }
        else{
            return response()->json(['Message'=>"Delete Succesful",'Code'=>200],200);
        }
    }
}
