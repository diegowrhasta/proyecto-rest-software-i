<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TheSeer\Tokenizer\Exception;
use Illuminate\Support\Facades\Validator;
use App\Http\BL\TestBL;
use App\Http\DAO\TestDao;

class TestingController extends Controller
{
    public function store(Request $request){
        try{
            if(count($request->json()->all())==3){
                $validator = Validator::make($request->json()->all(), [
                    'name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'age' => 'required|numeric',
                    ]);
                if($validator->fails()){
                        return response()->json($validator->errors()->toJson(), 400);
                }
                else{
                    $test = $request->json()->all();
                    $testDAO = new TestDao;
                    $testBL = new TestBL($testDAO);

                    $testBL->setName($test['name']);
                    $testBL->setLast_name($test['last_name']);
                    $testBL->setAge($test['age']);

                    $testBL->da_last_name();
                    $testBL->da_name();
                    if($testBL->sendModel()){
                        return response()->json(['Message'=>'Test Model Created','code'=>201],201);
                    }
                    else{
                        return response()->json(['Message'=>'Test Model Not Created','code'=>400],400);
                    }
                    /*return response()->json([
                        'name'=>$testBL->getName(),'last_name'=>$testBL->getLast_name(),'age'=>$testBL->getAge(),'message'=>$testBL->testClass()]
                        ,200);*/
                }
            }
            else{
                return response()->json(['Message'=>'Invalid Values','Code'=>400],400);
            }
            
        }
        catch(Exception $e){
            return response()->json(['Message'=>'Error'],400);
        }
    }
    public function index(){
        $testDao = new TestDao;
        $testModels = $testDao->getAll();
        $count = $testModels->count();
        if($count<1){
            return response()->json(['Message'=>'No Test Models registered','Code'=>404],404);
        }
        else{
            return response()->json(['data'=>$testModels],200);
        }
    }
    public function show($id_test_model){
        $testDao = new TestDao;
        if(!$testDao->getOne($id_test_model)){
            return response()->json(['Message'=>'Test Model Not Found','Code'=>404],404);
        }
        else{
            return response()->json(['data'=>$testDao->getOne($id_test_model)],200);
        }
    }
    public function update(Request $request,$id_test_model){
        $testDao = new TestDao;
        if($testDao->getOne($id_test_model) || (count($request->json()->all())==3)){
            $testModel = $testDao->getOne($id_test_model);
            $testModel -> name = $request->json('name');
            $testModel -> last_name = $request->json('last_name');
            $testModel -> age = $request->json('age');
            if(!$testDao->editOne($testModel)){
                return response()->json(['Message'=>'Could not edit Test Model','Code'=>500],500);
            }
            else{
                return response()->json(['Message'=>'Test Model edit successful','Code'=>200],200);
            }
        }
        else{
            return response()->json(['Message'=>'Test Model Not Found','Code'=>404],404);
        }
    }
    public function destroy($id_test_model){
        $testDao = new TestDao;
        $testBL = new TestBL($testDao);
        if(!$testDao->getOne($id_test_model)){
            return response()->json(['Message'=>'Test Model Not Found','Code'=>404],404);
        }
        else{
            if($testBL -> checkDependencies($id_test_model)){
                return response()->json(['Message'=>'Delete Successful','Code'=>200],200);
            }
            else{
                return response()->json(['Message'=>'Delete Failed','Code'=>500],500);
            }
        }
    }
}
