<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\package_sale;
use App\Claim;
use Validator;
class Package_saleClaimController extends Controller
{ 
    public function index($id_package_sale)
    {
        $package_sale=package_sale::find($id_package_sale);
        $claim=$package_sale->claim;
          if(sizeof($claim)>0){
            return response()->json(['Message'=>'Not Found Error Package_sale','code'=>404],404);
        }
        return response()->json(['data'=>$claim],200);
    }

  
    public function store(Request $request,$id_package_sale){
       $validator = Validator::make($request->all(), [
            'commentary' => 'required|string|max:100'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(),400);
        }
        $package_sale=package_sale::find($id_package_sale);
        if(!$package_sale){
            return response()->json(['Message'=>'Not Found Error Package_sale','code'=>404],404);
       }
       Claim::create([
           'commentary'=>$request->get('commentary'),
           'id_package_sale'=>$id_package_sale
       ]);
           return response()->json(['Message'=>'Created Package_sale','code'=>202],202);

    }

    public function update(Request $request,$id_package_sale, $id_claim){
        $metodo=$request->method();
        $validator = Validator::make($request->all(), [
            'commentary' => 'string|max:100'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(),400);
        }
        $package_sale=package_sale::find($id_package_sale);
        if(!$package_sale){
            return response()->json(['Message'=>'Not Found Error','code'=>404],404);
        }
        $claim=$package_sale->claim()->find($id_claim);
        if(!$claim){
            return response()->json(['Message'=>'Not Found Error','code'=>404],404);
        }
 
        $commentary= $request->get('commentary');
        
        $flag= false;
        if($metodo==="PATCH"){
            if($commentary!=null && $commentary!=''){
                $claim->commentary=$commentary;
                $flag= true;
            }
           
             if($flag){
                 $claim->save();
                 return response()->json(['Message'=>'Edit Complete, patch'],200);
         }
         return response()->json(['Message'=>'Not save for, PATCH'],304);
        }
      
        if(!$commentary ){
            return response()->json(['Message'=>'invalid data error'],304);
        }
        $claim->commentary=$commentary;
        
        $claim->save();
        return response()->json(['Message'=>'edit complete,  put'],202);
         }
 
     public function destroy($id_package_sale, $id_claim){
        $package_sale=package_sale::find($id_package_sale);
         if(!$package_sale){
            return response()->json(['Message'=>' package_sale dont found','code'=>404],404);
        }
         $claim=$package_sale->claim()->find($id_claim);
         if(!$claim){
            return response()->json(['Message'=>' claim dont found','code'=>404],404);
        }
         $claim->delete();
         return response()->json(['Message'=>' claim deleted','code'=>204],204);
      }
}
