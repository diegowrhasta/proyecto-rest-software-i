<?php

namespace App\Http\Controllers\Package;
use App\Http\Controllers\Controller;
use App\Http\Resources\PackageResource;
use App\Package;
use Illuminate\Http\Request;
use Validator;
use App\Http\BL\PackageBL;
use App\Http\DAO\PackageDAO;
use Symfony\Component\HttpFoundation\Response;
class PackageController extends Controller
{
    
    
    public function index()
    {
        $packageDAO = new PackageDAO;
        $package = $packageDAO->getAllpackage();
        
        if(sizeof($package)>0){
        return $this->showAll($package);
        }
        return $this->errorResponse('not exist: '.$package,404);
    }

    
     public function store(Request $request){
        $rules = [
            'name' =>'required|string|max:60',
            'price' =>'required|numeric',
            'start_date' =>'required|date_format:"Y-m-d"|before:end_date',
            'end_date' =>'required|date_format:"Y-m-d"|after:start_date',
            'max_adults' =>'required|integer|max:40|min:1',
            'max_children' =>'required|integer|max:20',
            'creation_date' =>'required|date_format:"Y-m-d"|before:start_date',
            'score' =>'required|numeric|min:1|max:10'
        ];

        $data = $this->transformAndValidateRequest(PackageResource::class, $request, $rules);
        $packageBL = new PackageBL;
        $response=$packageBL->createPackage($data);
     
       return $this->showOne($response,201);
      
     }

    
    
    public function show($id_package)
    {
        $packageDAO = new PackageDAO;
        $package = $packageDAO->getOnepackage($id_package);
        if(!$package){
            return $this->errorResponse('not exist: '.$package,404);}
        return $this->showOne($package);
    }

    
    public function edit($id_package)
    {
        return "Edit".$id_package;
    }

   
    public function update(Request $request, $id_package)
    {
        $rules = [
            'name' =>'string|max:60',
            'price' =>'numeric',
            'start_date' =>'date_format:"Y-m-d"|before:end_date',
            'end_date' =>'date_format:"Y-m-d"|after:start_date',
            'max_adults' =>'integer|max:40|min:1',
            'max_children' =>'integer|max:20',
            'creation_date' =>'date_format:"Y-m-d"|before:start_date',
            'score' =>'numeric|min:1|max:10'
        ];

        $package=Package::find($id_package);
       
        if(is_null($package)){
            return $this->errorResponse('not exist: '.$package,404);}

        $data = $this->transformAndValidateRequest(PackageResource::class, $request, $rules);
        $package->fill($data);
 
        if($package->isClean()){
            return $this->errorResponse("You need to specify any new value to update the package",422);}
        
        $packageBL = new PackageBL;
        $packageBL->updateBL($package,$id_package);
        
        return $this->showOne($package);
      
    }

    
    public function destroy($id_package)
    {
        $packageBL = new PackageBL;
        $package=Package::find($id_package);
        if(is_null($package)){
            return $this->errorResponse('not exist: '.$package,404);}
         $packageBL->destroyBL($id_package);
        return $this->showOne($package);
    }
}
