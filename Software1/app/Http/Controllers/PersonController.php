<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\BL\PersonBL;
use App\Http\DAO\PersonDAO;
use PHPUnit\Framework\Exception;

class PersonController extends Controller
{ 
    public function store(Request $request){
        // Check for presence of a body in the request
        try{
            if(count($request->json()->all())==10){
                $validator = Validator::make($request->json()->all(),[
                    'name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'b_day' => 'required|string',
                    'address' => 'required|string',
                    'phone' => 'required|numeric',
                    'gender' => 'required|string',
                    'country' => 'required|string',
                    'city' => 'required|string',
                    'id_img' => 'required|numeric|unique:people',
                    'ID_NIT' => 'required|numeric',
                    ]);
                if($validator->fails()){
                        return response()->json($validator->errors()->toJson(), 400);
                }
                else{
                    $person = $request->json()->all();

                    $personBL = new PersonBL;

                    if($personBL->sendPerson($person)){
                        return response()->json(['Message'=>'Person added','Code'=>201],201);
                    }
                    else{
                        return response()->json(['Message'=>'Person not added','Code'=>400],400);
                    }
                }
            }
            else{
                return response()->json(['Message'=>'Invalid Data','Code'=>400],400);
            }
        }
        catch(Exception $e){
            return response()->json(['Message'=>$e->getMessage()],500);
        }
    }
    public function index(){
        $personDAO = new PersonDAO;
        $people = $personDAO->getPeople();
        $count = $people->count();
        if($count<1){
            return response()->json(['Message'=>'No People Registered','Code'=>404],404);
        }
        else{
            return response()->json(['data'=>$people],200);
        }
    }
       
    public function show($id_person){
        $personDAO = new PersonDAO;
        if(!$personDAO->getPerson($id_person)){
            return response()->json(['Message'=>'Person Not Found','Code'=>404],404);
        }
        else{
            return response()->json(['data'=>$personDAO->getPerson($id_person)],200);
        }
    }
    public function update(Request $request,$id_person){
        $personBL = new PersonBL; 
        $person_new = $request->json()->all();
        $validator = Validator::make($person_new,[
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'b_day' => 'required|string',
            'address' => 'required|string',
            'phone' => 'required|numeric',
            'gender' => 'required|string',
            'country' => 'required|string',
            'city' => 'required|string',
            'id_img' => 'required|numeric|unique:people',
            'ID_NIT' => 'required|numeric',
            ]);
        if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
        }
        else{
            if($personBL->prepareUpdate($person_new,$id_person) && (count($person_new))==10){
                return response()->json(['Message'=>'Person edited','Code'=>200],200);
            }
            else{
                return response()->json(['Message'=>'Person not edited','Code'=>400],400);
            }
        }    
    }
    public function destroy($id_person){
        //BL
        $PersonBL = new PersonBL;
        if(!$PersonBL->prepareDestroy($id_person)){
            return response()->json(['Message'=>"Delete Failed",'Code'=>400],400);
        }
        else{
            return response()->json(['Message'=>"Delete Succesful",'Code'=>200],200);
        }
    }
}
