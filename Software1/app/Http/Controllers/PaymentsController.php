<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Payment;
use App\Enterprise_subscription;
use App\Enterprise;

class PaymentsController extends Controller
{
    public function create(Request $request){
       $id_enterprise_subscription=$request->json('id_enterprise_subscription');
        
        $enterprise_subscriptions=Enterprise_subscription::find($id_enterprise_subscription);
        
        if(!$enterprise_subscriptions){

            return response()->json(['Message'=>'Enterprise Subscription not found','code'=>404],404);
        }
        
        $validator = Validator::make($request->json()->all(), [
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'card_number' => 'required|numeric',
            'expiration_date' => 'required|numeric',
            'security_code' => 'required|numeric',
            'payment_format' => 'required|string',
               ]);
  
      if($validator->fails()){
              return response()->json($validator->errors()->toJson(), 400);
      }
      else{
            $fecha = date("Y-m-d");
            $fecha = strtotime('+30 day',strtotime($fecha));
            $fecha = date('Y-m-d',$fecha);
            $payment = Payment::create([
            'total' =>  200,  
            'state' =>  1,
              'description' => 'pago de suscripcion',
              'name' =>  $request->json('name'),
              'last_name' =>  $request->json('last_name'),
              'card_number' => $request->json('card_number'),
              'expiration_date' => $request->json('expiration_date'),
              'security_code' => $request->json('security_code'),
              'payment_format' =>  $request->json('payment_format'),
              'pay_day' => now(),
              'next_payment_day' => $fecha,
              'service_period'=>'29',
              'id_enterprise_subscription' =>  $request->json('id_enterprise_subscription'), 
              
          ]);
    
        }  
    
    return response()->json(['Message'=>'Pago realizado con exito!', 'payment'=>$payment,'code'=>201],200);
    }
   
    
    
    public function show($id_payment)
    {
        $payment=Payment::find($id_payment);
        if(!$payment){
            return response()->json(['Message'=>'Payment error','code'=>404],404);
        }
        return response()->json(['data_show'=>$payment],200);
    }

    public function all()
    {
         $payments=Payment::all();
        if(!$payments){
            return response()->json(['Message'=>'Payment error','codigo'=>404],404);
        }
        return response()->json(['data_all'=>$payments],200);
    }
    
    public function list($id_enterprise_subscription)
    {
         $payments=Payment::whereRaw('id_enterprise_subscription = ?',[$id_enterprise_subscription])->get();
        if(!$payments){
            return response()->json(['Message'=>'Payments error','codigo'=>404],404);
        }
        return response()->json(['data_all'=>$payments],200);
    }
   
    public function update(Request $request, $id_payment)
    {
        $payment=Payment::find($id_payment);
        if(!$payment){
            return response()->json(['Message'=>'Payment error','codigo'=>404],404);
        }

        $payment->state = $request->json('state');
        
        
        $payment->save();

        return response()->json(['data_show'=>$payment],200);
        
    }

    
   

}
