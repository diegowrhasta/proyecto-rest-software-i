<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Claim;
class ClaimController extends Controller
{
    public function index()
    {
        $claim=Claim::all();
        if(sizeof($claim)>0){
            return response()->json(['Message'=>'Not Found Error','code'=>404],404);
        }
        return response()->json(['data'=>$claim],200);
    }
   public function show($id_claim)
    {
        $claim=Claim::find($id_claim);
        if(!$claim){
            return response()->json(['Message'=>'Not Found Error Claim','code'=>404],404);
        }
        return response()->json(['data'=>$claim],200);
    }

}
