<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Activity;
use App\Package;
use App\Http\Resources\ActivityResource;
use Validator;
use Symfony\Component\HttpFoundation\Response;
class ActivityController extends Controller
{
    public function index($id_package)
    {
        $package=Package::find($id_package);
        if(is_null($package)){
            return $this->errorResponse('not exist: '.$package,404);}
        $data=$package->activity;
        if(sizeof($data)>0){
            return $this->showAll($data);
            }
            return $this->errorResponse('dont have: '.$data,404);
    }

    
    public function create($id_activity)
    {
       return "Create".$id_activity;
   }

    public function store(Request $request,$id_package)
    {
        $package=Package::find($id_package);
        if(is_null($package)){
            return $this->errorResponse('not exist: '.$id_package,404);}

        $rules = [
        'name' =>'required|string|max:60' ,
        'date' => 'required|date_format:"Y-m-d"',
        'time_start' => 'required|date_format:H:i:s',
        'time_end' => 'required|date_format:H:i:s|after:time_start',
        'description' =>'required|string|max:250'
        ];
        
        $data = $this->transformAndValidateRequest(ActivityResource::class, $request, $rules);
        $activity = new Activity($request->all());
        $package->activity()->save($activity);

        return $this->showOne($activity,201);
    }
  

    public function show($id_activity)
    {
     //show
    }
    public function update(Request $request,$id_package,$id_activity)
    {
        $rules = [
        'name' =>'string|max:60' ,
        'date' => 'date_format:"Y-m-d"',
        'time_start' => 'date_format:H:i:s',
        'time_end' => 'date_format:H:i:s|after:time_start',
        'description' =>'string|max:250'
        ];
        $package=Package::find($id_package);    
        $activity=Activity::find($id_activity);
        $data=$package->activity()->find($id_activity);
       
        if((is_null($package)) || (is_null($activity))|| (!$data)){
            return $this->errorResponse('not exist: '.$id_package.' on/or '.$id_activity ,404);}

        $data = $this->transformAndValidateRequest(ActivityResource::class, $request, $rules);
        $activity->fill($data);
 
        if($activity->isClean()){
            return $this->errorResponse("You need to specify any new value to update activity",422);}

        $activity->update($request->all());
        return $this->showOne($activity,201);
    }

    public function destroy($id_package,$id_activity)
    {
        
       $package=Package::find($id_package);
       $activity=Activity::find($id_activity);
       $data=$package->activity()->find($id_activity);
       if((is_null($package)) || (is_null($activity))|| (!$data)){
        return $this->errorResponse('not exist: '.$id_package.' on/or '.$id_activity,404);}

       $activity->delete();
        return $this->showAll($package->activity);
    }
   
  
}
