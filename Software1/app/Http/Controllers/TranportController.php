<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tranport;
class TranportController extends Controller
{
    public function index()
    {
        $Tranport = Tranport::all();
        if($Tranport!=null){
            return response()->json(['Message'=>"No Transports Registered",'error_code'=>404],404);
        }
        else{
            return response()->json(['data'=>$Tranport],200);
        }
    }
    public function store(Request $request)
    {
        if($request->get('id_transport_type')==null||$request->get('id_transport_enteprise')==null)
        {
            return response()->json(['Message'=>'Bad Input','code',400],400);
        }
        else
        {
            Tranport::create($request->json()->all());
            return response()->json(['Message'=>'Transport Added','code',201],201);
        }

    }
    public function show($id_transport)
    {
        $tranport = Tranport::find($id_transport);
        if(!$tranport){
            return response()->json(['Message'=>"Transport not found",'error_code'=>404],404);
        }
        else{
            return response()->json(['data'=>$tranport],200);
        }
    }
}
