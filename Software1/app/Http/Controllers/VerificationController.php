<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VerificationController extends Controller
{   
    public function index(){
        $verificator=Verificator::all();
        if(sizeof($verificator)<1){
            return response()->json(['Message'=>'No Verifications Found','error_code'=>404],404);
        }
        return response()->json(['data'=>$verificator],200);
    }
    public function store(Request $request){
        if(!$request->get('result')||!$request->get('observation')||!$request->get('date')||!$request->get('id_verificator')||!$request->get('id_user') ){
            return response()->json(['Message'=>'Invalid Data','error_code'=>404],404);
        }
        $verificator = Verificator::find($request->get('id_verificator'));
        $user = User::find($request->get('id_user'));
        if(!$verificator){
            return response()->json(['Message'=>'Verificator does not exist','error_code'=>404],404);
        }
        else if(!$user){
            return response()->json(['Message'=>'User does not exist','error_code'=>404],404);
        }   
        Verificator::create($request->all());
            return response()->json(['Message'=>'Verification Added','code',201],201);
    }
}
