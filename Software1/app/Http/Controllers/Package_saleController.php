<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\package_sale;
use Validator;
use App\Http\Resources\Package_saleResource;
use App\Http\Controllers\Controller;
use App\Http\BL\Pack_saleBL;
use App\Http\DAO\Pack_saleDAO;
class Package_saleController extends Controller
{
    
    public function index()
    {
         $package_sale=package_sale::all();
        if(!$package_sale){
            return $this->errorResponse('Not Found Error '.$package_sale,404);
        }
        return $this->showAll($package_sale);
    }

    public function store(Request $request){
         $pack = $this->transformAndValidateRequest(Package_saleResource::class, $request, [
            'raw_amount' => 'required|numeric',
            'sale_date' => 'required|date_format:"Y-m-d"',
            'score' => 'required|integer|max:10|min:1',
            'comment' => 'required|string|max:100',
            'id_tourist' => 'required|integer',
            'id_package' => 'required|integer'
        ]);
        $packbl = new Pack_saleBL;
        $data=$packbl->createPack_Sale($pack);
     
       return $this->showOne($data,201);

    }

    public function show($id_package_sale)
    {
        $packdao = new Pack_saleDAO;
        $pack = $packdao->getPackSale($id_package_sale);
        if(!$pack){
            return $this->errorResponse('Not Found Error '.$pack,404);}
        else{   
            return $this->showOne($pack);
        }
    }

   
    
    public function update(Request $request, $id_package_sale)
    { 
       $pack = $this->transformAndValidateRequest(Package_saleResource::class, $request, [
            'raw_amount' => 'numeric',
            'sale_date' => 'date_format:"Y-m-d"',
            'score' => 'integer|max:10|min:1',
            'comment' => 'string|max:100',
            'id_tourist' => 'integer',
            'id_package' => 'integer'
        ]);
       $package_sale=package_sale::find($id_package_sale);
       if(!$package_sale){
        return $this->errorResponse('Not Found Error '.$package_sale,404);
       }
       $package_sale->fill($pack);
       $packbl = new Pack_saleBL;
       $packbl->updatePack_Sale($package_sale,$id_package_sale);
       return $this->showOne($package_sale);
 
    }

   
    public function destroy($id_package_sale)
    {
      $packdao = new Pack_saleDAO;
        $packbl = new Pack_saleBL;
        $package_sale = $packdao->getPackSale($id_package_sale);
       if(!$package_sale){
        return $this->errorResponse('package_sale dont found '.$package_sale,404);
         }

        $claim=$package_sale->claim;

         if(sizeof($claim)>0){
            return $this->errorResponse('cant delete .... claim detected...'.$package_sale,400);
             
         }
         $packbl->deletePack_Sale($id_package_sale);
         return $this->showOne($package_sale);
    
    }
}
