<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = $this->create($request->all());
        event(new Registered($user));

        $inputs = $request->all();
        
        $id_user=$user->id;
    
        $estado = DB::table('people')->insert([
            'name' => $inputs['name'], 
            'last_name' => $inputs['b_day'], 
            'addres' => $inputs['addres'], 
            'phone' => $inputs['phone'],
            'gender' => $inputs['gender'],
            'country' => $inputs['country'],
            'city' => $inputs['city'],
            'e-mail' => $inputs['e-mail'],
            'id_img' => $inputs['id_img'],
            'ID_NIT' =>$inputs['ID_NIT'], 
            'id_user' => $id_user , 
        ]);
        
       

        

    }
    public function select($id)
    {
        $tipo = DB::table('tipo_usuario')->get();  
        return view('auth/register',['proyecto'=>$tipo]);   
    }
}
