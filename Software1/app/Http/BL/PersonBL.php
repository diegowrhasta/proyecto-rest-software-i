<?php
namespace App\Http\BL;

use App\Http\DAO\PersonDAO;
use App\Http\DAO\UserDAO;
use PHPUnit\Framework\Exception;

class PersonBL
{
    function sendPerson($person){
        try{
            $PersonDAO = new PersonDAO;
            if($PersonDAO->insertPerson($person)){
                return true;
            }
            else{
                return false;
            }
        }
        catch(\Exception $e){
            return false;
        } 
    }
    function prepareUpdate($person_new,$id_person){
        $PersonDAO = new PersonDAO;
        if($PersonDAO->getPerson($id_person)){
            $person_old=$PersonDAO->getPerson($id_person);
            $person_old -> name = $person_new['name'];
            $person_old -> last_name = $person_new['last_name'];
            $person_old -> b_day = $person_new['b_day'];
            $person_old -> address  = $person_new['address'];
            $person_old -> phone = $person_new['phone'];
            $person_old -> gender = $person_new['gender'];
            $person_old -> country = $person_new['country'];
            $person_old -> city = $person_new['city'];
            $person_old -> id_img = $person_new['id_img'];
            $person_old -> ID_NIT = $person_new['ID_NIT'];
            $PersonDAO->editPerson($person_old);
            return true;
        }
        else{
            return false;
        }
    }
    function prepareDestroy($id_person){
        $PersonDAO = new PersonDAO;
        if(!$PersonDAO->getPerson($id_person)){
            return false;
        }
        else{
            $UserDAO = new UserDAO;
            $user = $UserDAO->getUserwithAttrib('id_person',$id_person);
            if(!$user){
                return false;
            }
            else{
                if($PersonDAO->deletePerson($id_person) && $UserDAO->deleteUser($user->id_user)){
                    return true;
                }
                else{
                    return false;
                }
            }
        }
    }
}