<?php
namespace App\Http\BL;

use App\Http\DAO\TranportDAO;
class TranportBL
{
    function sendTranport($Tranport){
        $TranportDAO = new TranportDAO;
        $Tranport_created = $TranportDAO->insertTranport($Tranport);
        if(!$Tranport_created){
            return false;
        }
        else{
            return $Tranport_created;
        }
    }
    function prepareDestroy($id_Tranport){
        $TranportDAO = new TranportDAO;
        if($id_Tranport){
            if($TranportDAO->deleteTranport($id_Tranport)){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
}
