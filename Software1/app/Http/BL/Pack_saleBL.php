<?php
namespace App\Http\BL;

use App\Http\DAO\Pack_saleDAO;
use App\package_sale;
class Pack_saleBL
{
    function createPack_Sale($package_sale){
        
        $pack = new Pack_saleDAO;
        $pack_sale = $pack->createPackSale($package_sale);
        if($pack_sale){
            return $pack_sale;
        }return false;
    }

    function updatePack_Sale($package_sale,$id_package_sale){
        $pack = new Pack_saleDAO;
        if($pack->getPackSale($id_package_sale)){
            $pack_s = $pack -> getPackSale($id_package_sale);
            $pack_s -> raw_amount = $package_sale['raw_amount'];
            $pack_s -> sale_date = $package_sale['sale_date'];
            $pack_s -> score = $package_sale['score'];
            $pack_s -> comment  = $package_sale['comment'];
            $pack_s -> id_tourist = $package_sale['id_tourist'];
            $pack_s -> id_package = $package_sale['id_package'];
            $pack->savePackSale($pack_s);
            return true;
        }else{
            return false;
        }
    }
    function deletePack_Sale($id_package_sale){
        $pack = new Pack_saleDAO;
        $package_sale=$pack->getPackSale($id_package_sale);
       if(sizeof($package_sale->claim)>0){
        return false;
       }else{
       $pack->deletePackSale($id_package_sale);
       return true;}
    }
}