<?php
namespace App\Http\BL;

use App\Http\DAO\TestDao;
class TestBL
{
    private $TestDAO;
    private $name;
    private $last_name;
    private $age;

    public function __construct(TestDao $TestDAO){
        $this->TestDAO = $TestDAO;
    }
    function da_name(){
        $this->name = 'Number: '.$this->name ;
    }
    function da_last_name(){
        $this->last_name = 'Subject: '.$this->last_name;
    }

    function do_foo($foo){
        echo 'Hi '.$foo;
    }
    function setName($name){
        $this->name = $name;
    }
    function getName(){
        return $this->name;
    }
    function setLast_name($last_name){
        $this->last_name = $last_name;
    }
    function getLast_Name(){
        return $this->last_name;
    } 
    function setAge($age){
        $this->age = $age;
    }
    function getAge(){
        return $this->age;
    }

    function sendModel(){
        if($this->TestDAO->insert_Test($this->name,$this->last_name,$this->age)){
            return true;
        }
        else{
            return false;
        }
    }
    function checkDependencies($id_test_model){
        //Just passing by brother
        if($this->TestDAO->deleteOne($id_test_model)){
            return true;
        }
        else{
            return false;
        }
    }
}