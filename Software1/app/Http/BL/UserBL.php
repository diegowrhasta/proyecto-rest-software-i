<?php
namespace App\Http\BL;

use App\Http\DAO\UserDAO;
use App\Http\DAO\PersonDAO;
use PHPUnit\Framework\Exception;
use App\User;
use \Firebase\JWT\JWT;
use Illuminate\Support\Facades\Hash;
class UserBL
{
    function sendUser($user){
        $userDAO = new UserDAO;
        $user['password']=Hash::make($user['password'].env('Salt'));
        $user_created = $userDAO->insertUser($user);
        if(!$user_created){
            return false;
        }
        else{
            return $user_created;
        }
    }
    function checkUser($credentials){
        try{
            $userDAO = new UserDAO;
            $user = $userDAO->getUserwithAttrib('username',$credentials['username']);
            if(!$user || $user->deleted_at){
                return false;
            }
            else{
                $credentials['password']=$credentials['password'].env('Salt');
                if (Hash::check($credentials['password'], $user->password)) {
                    $key = env('JWT_SECRET');
                    //Bearer Token
                    $token_raw1 = array(
                        "sub" => ($user->id_user),
                        "iat" => (time()),
                        "exp" => (time() + 600),
                        "type" => "bearer"
                    );
                    $bearer_token = JWT::encode($token_raw1, $key);
                    //Refresh Token
                    $token_raw2 = array(
                        "sub" => ($user->id_user),
                        "iat" => (time()),
                        "exp" => (time() + 1500),
                        "type" => "refresh"
                    );
                    $refresh_token = JWT::encode($token_raw2, $key);
                    return json_encode(['bearer_token'=>$bearer_token,'refresh_token'=>$refresh_token]);
                }
                else{
                    return false;
                }
            } 
        }
        catch(\Exception $e){
            return false;
        }    
    }
    function prepareShow($id_person,$id_user){
        $personDAO = new PersonDAO;
        if(!$personDAO->getPerson($id_person)){
            return false;
        }
        else{
            $userDAO = new UserDAO;
            $check_owner = $userDAO->getUserwithAttrib('id_person',$id_person);
            $user = $userDAO->getUser($id_user);
            if(!$user){
                return false;
            }
            else if($check_owner->id_person == $user->id_person){
                return $check_owner;
            }
        }
    }
    function prepareUpdate($user_new,$id_user){
        $userDAO = new UserDAO;
        $user_old = $userDAO->getUser($id_user);
        $count = count($user_new);
        if($user_old && ($count==5)){
            $new_password = Hash::make($user_new['password'].env('Salt'));
            $user_old -> username = $user_new['username'];
            $user_old -> email = $user_new['email'];
            $user_old -> password = $new_password;
            $user_old -> id_person = $user_new['id_person'];
            if(!$userDAO->editUser($user_old)){
                return false;
            }
            else{
                return true;
            }
        }
        else{
            return false;
        }
    }
    function prepareDestroy($id_person,$id_user){
        $userDAO = new UserDAO;
        $personDAO = new PersonDAO;
        $person = $personDAO->getPerson($id_person);
        $user = $userDAO->getUserwithAttrib('id_person',$person->id_person);
        if($person && $user){
            if($personDAO->deletePerson($id_person) && $userDAO->deleteUser($id_user)){
                return true;
            }
            else{
                return false;
            }
        }
        else{   
            return false;
        }
    }
}