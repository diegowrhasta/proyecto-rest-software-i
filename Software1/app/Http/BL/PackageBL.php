<?php
namespace App\Http\BL;

use App\Http\DAO\PackageDAO;
use App\Package;
class PackageBL
{
    function createPackage($package){
        
        $packageDAO = new PackageDAO;
        $packageC = $packageDAO->addPackage($package);
        if(!$packageC){
            return false;
        }
        else{
            return $packageC;
        }
    }
    function updateBL($package,$id_package){
        $PackageDAO = new PackageDAO;
        if($PackageDAO->getOnepackage($id_package)){
            $pack=$PackageDAO->getOnepackage($id_package);
            $pack -> name = $package['name'];
            $pack -> price = $package['price'];
            $pack -> start_date = $package['start_date'];
            $pack -> end_date  = $package['end_date'];
            $pack -> max_adults = $package['max_adults'];
            $pack -> max_children = $package['max_children'];
            $pack -> creation_date = $package['creation_date'];
            $pack -> score = $package['score'];
            $PackageDAO->savePackage($pack);
            return true;
        }
            return false;
        
    }
    function destroyBL($id_package){
        $PackageDAO = new PackageDAO;
        $package=$PackageDAO->getOnepackage($id_package);
       if((sizeof(($package->activity))>0)){
        return false;
       }
       $PackageDAO->destroyPackage($id_package);
       return true;
    }
}