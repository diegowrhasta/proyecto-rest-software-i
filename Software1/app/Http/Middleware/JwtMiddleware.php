<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use \Firebase\JWT\JWT;
use App\Http\DAO\UserDAO;
use Firebase\JWT\ExpiredException;
use League\Flysystem\Exception;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            //Get JWT
            $jwt = $request->header('Authorization');
            if(!isset($jwt)){
                return response()->json(['message' => 'No Token','Code' => 404],404);
            }
            $splitted = explode(" ",$jwt);
            $token = $splitted[1];
            //Retrieve JWT Key
            $key = env('JWT_SECRET');
            //Decode the JWT
            $decoded = JWT::decode($token, $key, array('HS256'));

            $decoded_array = (array) $decoded;

            //return response()->json($decoded_array['sub']);
            //Look for User
            $userDAO = new UserDAO;
            $user = $userDAO->getUser($decoded_array['sub']);
            //User::find($decoded_array['sub']);
            if(!$user){
                return response()->json(['Message' => 'Invalid Token','Code' => 400],400);
            }
            else{
                if($decoded_array['type']=='bearer'){
                    if($decoded_array['exp']>time()){
                        return $next($request);
                    }
                    else{
                        return response()->json(['Message' => 'Token Expired','Code' => 307],307);
                    }
                }
                else if($decoded_array['type']=='refresh'){
                    if($decoded_array['exp']>time()){

                        //Bearer Token
                        $token_raw1 = array(
                            "sub" => ($decoded_array['sub']),
                            "iat" => (time()),
                            "exp" => (time() + 600),
                            "type" => "bearer"
                        );
                        $bearer_token = JWT::encode($token_raw1, $key);
                        //Refresh Token
                        $token_raw2 = array(
                            "sub" => ($decoded_array['sub']),
                            "iat" => (time()),
                            "exp" => (time() + 1500),
                            "type" => "refresh"
                        );
                        $refresh_token = JWT::encode($token_raw2, $key);
                        return response()->json(compact('user','bearer_token','refresh_token'),201);
                    }
                    else{
                        return response()->json(['Message' => 'Session Expired','Code' => 401],401);
                    }
                }
            }
        } 
        catch (ExpiredException $e) {
            return response()->json(['Message' => 'Token Expired','Code' => 307],307);
        }
        catch(\Exception $e){
            return response()->json(['Message' => 'Token Invalid','Code' => 400],400);
        }
    }
}
