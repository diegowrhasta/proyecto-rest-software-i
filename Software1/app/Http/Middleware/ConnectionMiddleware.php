<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\DAO\UserDAO;

class ConnectionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userDAO = new UserDAO;
        try{
            $userDAO->getUsers();
        }
        catch(\Exception $e){
            return response()->json(['Code'=>404,'Message'=>'DB Connection Failed'],404);
        }
        return $next($request);
    }
}
