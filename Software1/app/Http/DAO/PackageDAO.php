<?php
namespace App\Http\DAO;

use App\Package;
class PackageDAO
{
    function addPackage($package){
        
            $create = Package::create([
                'name' => $package['name'],
                'price' => $package['price'],
                'start_date' => $package['start_date'],
                'end_date' => $package['end_date'],
                'max_adults' => $package['max_adults'],
                'max_children' => $package['max_children'],
                'creation_date' => $package['creation_date'],
                'score' => $package['score'],
            ]);
    
            return $create;
       
    }
    function getAllpackage(){
        $package =  Package::all();
        return $package;
    }
    function getOnepackage($id_package){
        $package = Package::find($id_package);
        if($package){
            return $package;
        }
        else{
            return false;
        }  
    }
    function savePackage(Package $package){
        $aux=false;
        if($package){
            $package->save();
            $aux=true;
        }
            return $aux;
    }
    function destroyPackage($id_package){
        $aux=false;
        if($id_package){
            Package::destroy($id_package);
            $aux=true;
        }
            return $aux;
       
    }
}