<?php
namespace App\Http\DAO;

use App\Tranport;
use Illuminate\Support\Facades\DB;
class TranportDAO
{
    function insertTranport($Tranport){
        try{
            $created = Tranport::create([
                'id_transport_type' => $Tranport['id_transport_type'],
                'id_transport_enterprise' => $Tranport['id_transport_enterprise'],
            ]);
            return $created;
        }
        catch(\Exception $e){
            return false;
        }
    }
    function getTranport(){
        $Tranport =  Tranport::all();
        return $Tranport;
    }
    function deleteTranport($id_Tranport){
        try{
            Tranport::destroy($id_Tranport);
            return true;
        }
        catch(\Exception $e){
            return false;
        }
    }
    function getTranportwithAttrib($attrib,$value){
        try{
            $Tranport = DB::table('Tranport')->where($attrib, $value)->first();
            return $Tranport;
        }
        catch(\Exception $e){
            return false;
        }
    }
    function editTranport(Tranport $Tranport){
        try{
            $Tranport->save();
            return true;
        }
        catch (\Exception $e){
            return false;
        }
    }
}
