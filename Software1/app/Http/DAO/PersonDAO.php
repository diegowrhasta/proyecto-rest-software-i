<?php
namespace App\Http\DAO;

use App\Person;
use Mockery\Exception;

class PersonDAO
{
    function insertPerson($person){
        try{
            Person::create([
                'name' => $person['name'],
                'last_name' => $person['last_name'],
                'b_day' => $person['b_day'],
                'address' => $person['address'],
                'phone' => $person['phone'],
                'gender' => $person['gender'],
                'country' => $person['country'],
                'city' => $person['city'],
                'id_img' => $person['id_img'],
                'ID_NIT' => $person['ID_NIT'],
            ]);
            return true;
        }
        catch(\Exception $e){
            return false;
        }
    }
    function getPeople(){
        $people =  Person::all();
        return $people;
    }
    function getPerson($id_person){
        $person = Person::find($id_person);
        if($person){
            return $person;
        }
        else{
            return false;
        }  
    }
    function editPerson(Person $Person){
        try{
            $Person->save();
            return true;
        }
        catch (\Exception $e){
            return false;
        }    
    }
    function deletePerson($id_person){
        try{
            Person::destroy($id_person);
            return true;
        }
        catch(\Exception $e){
            return false;
        }
    }
}