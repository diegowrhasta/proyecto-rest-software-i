<?php
namespace App\Http\DAO;

use App\TestModel;
use Mockery\Exception;

class TestDao
{
    function insert_Test($name, $last_name, $age)
    {
        try{
            TestModel::create([
                'name' => $name,
                'last_name' => $last_name,
                'age' => $age,
            ]);
            return true;
        }
        catch(Exception $e){
            return false;
        }
    }
    function getAll(){
        $test_models =  TestModel::all();
        return $test_models;
    }
    function getOne($id_test_model){
        $test_model = TestModel::find($id_test_model);
        if($test_model){
            return $test_model;
        }
        else{
            return false;
        } 
    }
    function editOne(TestModel $testModel){
        try{
            $testModel->save();
            return true;
        }
        catch (Exception $e){
            return false;
        }    
    }
    function deleteOne($id_test_model){
        try{
            TestModel::destroy($id_test_model);
            return true;
        }
        catch (Exception $e){
            return false;
        }
    }
}