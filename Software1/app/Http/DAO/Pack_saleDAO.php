<?php
namespace App\Http\DAO;

use App\package_sale;
class Pack_saleDAO
{
    function createPackSale($package_sale){
        
            $data = package_sale::create([
                'raw_amount' => $package_sale['raw_amount'],
                'sale_date' => $package_sale['sale_date'],
                'score' => $package_sale['score'],
                'comment' => $package_sale['comment'],
                'id_tourist' => $package_sale['id_tourist'],
                'id_package' => $package_sale['id_package']                
            ]);
    
            return $data;
       
    }
    function savePackSale(package_sale $package_sale){
        
        if(!$package_sale){
            $flag=false;
        }else{
        $package_sale->save();
            $flag=true;
        }
        return $flag;
    }
    function deletePackSale($id_package_sale){
        if(!$id_package_sale){
            $flag=false;
        }else{
            package_sale::destroy($id_package_sale);
            $flag=true;
        }
        return $flag;
       
    }
    function getPackSale($id_package_sale){
        $pack = package_sale::find($id_package_sale);
        if($pack){
            return $pack;
        }
            return false;  
    }
}