<?php
namespace App\Http\DAO;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class UserDAO
{
    function insertUser($user){
        try{
            $created = User::create([
                'username' => $user['username'],
                'email' => $user['email'],
                'password' => $user['password'],
                'salt' => env('Salt'),
                'status' => 'active',
                'id_person' => $user['id_person'],
            ]);
            return $created;
        }
        catch(\Exception $e){
            return false;
        }
    }
    function getUsers(){
        $users =  User::all();
        return $users;
    }
    function deleteUser($id_user){
        try{
            User::destroy($id_user);
            return true;
        }
        catch(\Exception $e){
            return false;
        }
    }
    function getUserwithAttrib($attrib,$value){
        try{
            $user = DB::table('users')->where($attrib, $value)->first();
            return $user;
        }
        catch(\Exception $e){
            return false;
        }   
    }
    function getUser($id_user){
        try{
            $user = User::find($id_user);
            return $user;
        }
        catch(\Exception $e){
            return false;
        }
    }
    function editUser(User $user){
        try{
            $user->save();
            return true;
        }
        catch (\Exception $e){
            return false;
        }    
    }
}