<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Travel extends Model
{
    protected $table = 'travels';
    protected $primaryKey='id_travel';

    protected $fillable = [
        'duration',
        'distance',
        'final_travel',
        'first_travel',
        'id_location_start',
        'id_location_end',
        'id_transport',
        'id_package',
        'deleted_at',                          
    ];
}
