<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class package_image extends Model
{
    protected $table = 'package_images';
    protected $primaryKey='id_package_image';

    protected $fillable = [
        'id_package',
        'id_img',
        'deleted_at',                            
    ];
}
