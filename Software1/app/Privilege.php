<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    protected $table = 'privileges';
    protected $primaryKey='id_privilege';

    protected $fillable = [
        'name',
        'deleted_at',                            
    ];
}
