<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';
    protected $primaryKey='id_img';

    protected $fillable = [
        'url',
        'deleted_at',                            
    ];
}
