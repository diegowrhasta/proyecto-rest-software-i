<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tourist extends Model
{
    protected $table = 'tourists';
    protected $primaryKey='id_tourist';

    protected $fillable = [
        'id_user',
        'deleted_at',                            
    ];
    public function user(){
        return $this->belongsTo('App\User','id_user','id_user');
    }
}
