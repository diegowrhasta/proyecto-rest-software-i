<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Tranport_Enterprise extends Model
{
    use SoftDeletes;
    protected $table = 'transport_enterprises';
    protected $primaryKey='id_transport_enterprise';
    protected $softDelete = true;
    protected $fillable = [
        'name',
    ];
    public function Tranport()
    {
        return $this->hasMany('App\Tranport', 'id_transport', 'id_transport');
    }
}
