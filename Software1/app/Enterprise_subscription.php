<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enterprise_subscription extends Model
{
    use SoftDeletes;
    protected $table = 'enterprise_subscriptions';
    protected $primaryKey='id_enterprise_subscription';
    protected $softDelete = true;
    protected $fillable = [
       
        'id_enterprise',
        'state',
        'price',
    ];
    protected $hidden=['created_at','updated_at','deleted_at'];
    public function enterprise(){
        return $this->belongsTo('App\Enterprise','id_enterprise','id_enterprise');
    }
}
