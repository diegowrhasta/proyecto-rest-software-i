<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'locations';
    protected $primaryKey='id_location';

    protected $fillable = [
        'name',
        'deleted_at',                            
    ];
}
