<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Claim;
use App\Http\Resources\Package_saleResource;
class package_sale extends Model
{
    public $resource = Package_saleResource::class;
    protected $table = 'package_sales';
    protected $primaryKey='id_package_sale';

    protected $fillable = [
        'raw_amount',
        'sale_date',
        'score',
        'comment',
        'id_tourist',
        'id_package'                            
    ];
     protected $hidden = [
        'created_at','updated_at',
        'deleted_at'  
    ];
    public function claim(){
        return $this->hasMany(Claim::class);
     }
}
