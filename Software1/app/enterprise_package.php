<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class enterprise_package extends Model
{
    protected $table = 'enterprise_packages';
    protected $primaryKey='id_enterprise_package';

    protected $fillable = [
        'id_package',
        'id_enterprise',
        'deleted_at',                            
    ];
}
