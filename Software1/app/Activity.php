<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Package;    
use App\Http\Resources\ActivityResource;
class Activity extends Model
{
    
    public $resource = ActivityResource::class;
    protected $table = 'activities';
    protected $primaryKey='id_activity';

    protected $fillable = [
        'name',
        'date',
        'time_start',
        'time_end',
        'description',
        'package_id_package'                         
    ];
    protected $hidden = [
        "deleted_at" ,"created_at","updated_at" 
    ];
    public function package()
    {
        return $this->belongsTo(Package::class);
    }
}
