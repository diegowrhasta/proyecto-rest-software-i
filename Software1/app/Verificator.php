<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Verificator extends Model
{
    use SoftDeletes;
    protected $table = 'verificators';
    protected $primaryKey='id_verificator';
    protected $softDelete = true;
    protected $fillable = [
        'full_name',
        'username',
        'password',
        'salt',                          
    ];
    public function verification(){
        return $this->hasMany('App\verification','id_verificator','id_verificator');
    }
}
