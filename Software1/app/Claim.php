<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    protected $table = 'claims';
    protected $primaryKey='id_claim';

    protected $fillable = [
        'commentary',
        'package_sale_id_package_sale',
        'deleted_at',                            
    ];
    public function package_sale()
    {
        return $this->belongsTo('App\package_sale');
    }
}
