<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Activity;
use App\Http\Resources\PackageResource;
class Package extends Model
{
    protected $table = 'packages';
    protected $primaryKey='id_package';
    public $resource = PackageResource::class;
            
    protected $fillable = [
        'name',
        'price',
        'start_date',
        'end_date',
        'max_adults',
        'max_children',
        'creation_date',
        'score',                         
    ];
    protected $hidden = [
        "deleted_at" ,"created_at","updated_at" 
    ];
    public function activity(){
        return $this->hasMany(Activity::class);
     }
}
