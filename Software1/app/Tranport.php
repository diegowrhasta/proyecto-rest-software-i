<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tranport extends Model
{
    use SoftDeletes;
    protected $table = 'tranports';
    protected $primaryKey='id_transport';
    protected $softDelete = true;

    protected $fillable = [
        'id_transport_type',
        'id_transport_enterprise',
    ];
    protected $hidden=['created_at','updated_at','deleted_at'];

    public function Tranport_type()
    {
        return $this->belongsTo('App\Transport_Type', 'id_transport_type', 'id_transport_type');
    }
    public function Transport_enterprise()
    {
        return $this->belongsTo('App\Tranport_Enterprise', 'id_transport_enterprise', 'id_transport_enterprise');
    }

}
