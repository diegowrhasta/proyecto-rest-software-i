<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Traits\ApiResponser;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    use ApiResponser;
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($exception instanceof ValidationException)
        return $this->convertValidationExceptionToResponse($exception,$request);

    if($exception instanceof ModelNotFoundException)
    {
        $sModelName = class_basename($exception->getModel());
        return $this->errorResponse("No model found with name {$sModelName}",404); 
        //return $this->errorResponse($exception->getMessage(),404);
    }

    if($exception instanceof NotFoundHttpException)
        return $this->errorResponse("No endpoint found",$exception->getStatusCode());  

    if($exception instanceof MethodNotAllowedHttpException)
        return $this->errorResponse("Method not allowed",$exception->getStatusCode());       

    if($exception instanceof HttpException)
        return $this->errorResponse($exception->getMessage(),$exception->getStatusCode());        
        
    // El '.env' tiene 'APP_DEBUG = true' 
    if(config("app.debug"))
        return parent::render($request, $exception);
    
    return $this->errorResponse("Unexpected error",500);
    }
    protected function convertValidationExceptionToResponse(ValidationException $exception, $request)
    {
        $arErrors = $exception->validator->errors()->getMessages();
        
        return $this->errorResponse($arErrors,422);
       
    }
}
