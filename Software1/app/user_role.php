<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_role extends Model
{
    protected $table = 'user_role';
    protected $primaryKey='id_user_role';

    protected $fillable = [
        'id_user',
        'id_role',
        'deleted_at',                          
    ];
    public function user(){
        return $this->belongsTo('App\User','id_user','id_user');
    }
    public function role(){
        return $this->belongsTo('App\Role','id_role','id_role');
    }
}
