<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class H_User extends Model
{
    protected $table = 'h_users';
    protected $primaryKey='id_h_user';

    protected $fillable = [
        'username',
        'salt',
        'status',
        'id_person',
        'deleted_at',                            
    ];
}
