<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package_Offer extends Model
{
    protected $table = 'package_offers';
    protected $primaryKey='id_package_offer';

    protected $fillable = [
        'start_date',
        'end_date',
        'price_percentage',
        'status',
        'id_package',
        'deleted_at',                            
    ];
}
