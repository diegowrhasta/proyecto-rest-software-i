<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    protected $table = 'deposits';
    protected $primaryKey='id_deposit';

    protected $fillable = [
        'bank',
        'depositor',
        'trans_num',
        'date',
        'id_img',
        'deleted_at',
    ];
    public function enterprise_deposit()
    {
        return $this->hasMany('App\enterprise_deposit','id_deposit','id_deposit');
    }
}
