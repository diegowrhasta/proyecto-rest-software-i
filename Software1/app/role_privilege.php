<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class role_privilege extends Model
{
    protected $table = 'role_privileges';
    protected $primaryKey='id_role_privilege';

    protected $fillable = [
        'id_role',
        'id_privilege',
        'deleted_at',                            
    ];
}
