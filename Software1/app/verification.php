<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class verification extends Model
{
    protected $table = 'verifications';
    protected $primaryKey='id';

    protected $fillable = [
        'result',
        'observation',
        'date',
        'id_verificator',
        'id_user',
        'deleted_at',                          
    ];
    public function user(){
        return $this->belongsTo('App\User','id_user','id_user');
    }
    public function verificator(){
        return $this->belongsTo('App\Verificator','id_verificator','id_verificator');
    }
}
