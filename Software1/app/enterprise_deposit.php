<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class enterprise_deposit extends Model
{
    protected $table = 'enterprise_deposits';
    protected $primaryKey='id_enterprise_deposit';

    protected $fillable = [
        'id_deposit',
        'id_enterprise',
        'deleted_at',
    ];
    public function deposit(){
        $this->belongsTo('App\Deposit','id_deposit','id_deposit');
    }
    public function enterprise(){
        return $this->belongsTo('App\Enterprise','id_enterprise','id_enterprise');
    }
}
