<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    use SoftDeletes;
    protected $table = 'people';
    protected $primaryKey='id_person';
    protected $softDelete = true;
    protected $fillable = [
        'name',
        'last_name',
        'b_day',
        'address',
        'phone',
        'gender',
        'country',
        'city',
        'id_img',
        'ID_NIT',                          
    ]; 
    protected $hidden=['created_at','updated_at','deleted_at'];
    public function user(){
        return $this->hasMany('App\User','id_person','id_person');
    }
}
