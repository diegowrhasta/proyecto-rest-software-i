<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class H_Package extends Model
{
    protected $table = 'h_packages';
    protected $primaryKey='id_h_package';

    protected $fillable = [
        'id_package',
        'name',
        'price',
        'start_date',
        'end_date',
        'max_adults',
        'max_children',
        'creation_date',
        'score',
        'deleted_at',                            
    ];
}
