<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class H_Person extends Model
{
    protected $table = 'h_people';
    protected $primaryKey='id_h_people';

    protected $fillable = [
        'id_person',
        'name',
        'last_name',
        'b_day',
        'addres',
        'phone',
        'gender',
        'country',
        'city',
        'e-mail',
        'id_img',
        'ID-NIT',
        'deleted_at',                            
    ];
}
