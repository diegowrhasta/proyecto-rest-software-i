<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Enterprise extends Model
{   use SoftDeletes;
    protected $table = 'enterprises';
    protected $primaryKey='id_enterprise';
    protected $softDelete = true;
    protected $fillable = [
        'name',
        'func_lic',
        'score',
        'id_user',
       
    ];
    protected $hidden=['created_at','updated_at','deleted_at'];
    public function user(){
        return $this->belongsTo('App\User','id_user','id_user');
    }
}
