<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transport_Type extends Model
{
    protected $table = 'transport_type';
    protected $primaryKey='id_transport_type';

    protected $fillable = [
        'name',
        'deleted_at',
    ];
    public function Tranport()
    {
        return $this->hasMany('App\Tranport', 'id_transport', 'id_transport');
    }
}
