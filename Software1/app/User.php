<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
class User extends Authenticatable
{   
    use SoftDeletes;
    use Notifiable;
    protected $table = 'users';
    protected $primaryKey='id_user';
    protected $softDelete = true;
    protected $fillable = [
        'username',
        'email',
        'password',
        'salt',
        'status',
        'id_person',
        'email_verified_at'
    ];
    protected $hidden=['created_at','updated_at','deleted_at','password','remember_token','email_verified_at'];
    public function person(){
        return $this->belongsTo('App\Person','id_person','id_person');
    }
    public function enterprise(){
        return $this->hasMany('App\Enterprise','id_user','id_user');
    }
    public function user_role(){
        return $this->hasMany('App\user_role','id_user','id_user');
    }
    public function verification(){
        return $this->hasMany('App\verification','id_user','id_user');
    }
    public function tourist(){
        return $this->hasMany('App\Tourist','id_user','id_user');
    }   
}
