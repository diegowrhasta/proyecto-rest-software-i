<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Payment extends Model
{
    use SoftDeletes;
    protected $table = 'payments';
    protected $primaryKey='id_payment';
  
    protected $fillable = [
        'name',
        'last_name',
        'card_number',
        'expiration_date',
        'security_code',
        'pay_day',
        'next_payment_day',
        'description',
        'service_period',
        'payment_format',
        'id_enterprise_subscription',
        'state',
        'total',
    ];
    protected $hidden=['created_at','updated_at','deleted_at'];
    public function Enterprise_subscription()
    {
        return $this->belongsTo('App\Enterprise_subscription','id_enterprise_subscription','id_enterprise_subscription');
    }
}
