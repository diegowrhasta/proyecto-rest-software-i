<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TestModel extends Model
{
    use SoftDeletes;
    protected $table = 'test_models';
    protected $primaryKey='id_test_model';
    protected $softDelete = true;
    protected $fillable = [
        'name',
        'last_name',
        'age',
    ];
    protected $hidden=['created_at','updated_at','deleted_at'];
}
