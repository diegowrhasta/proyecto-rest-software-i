FORMAT: 1A
HOST: https://turismo.software.com

# Turismo API

Esta documentación es un borrador que se presentará el miércoles para Software, se usó como base las instrucciones del libro [Build APIs You Won't Hate].

[Build APIs You Won't Hate]: https://leanpub.com/build-apis-you-wont-hate

## Authorization

La autorización se hará en base a un Java Web Token (JWT) que será firmado por el servidor y se lo enviará a cada usuario (En realidad se mandarán 2 JWT uno para realizar las transacciones y otro para hacer un refresh del primero en caso de que expire).
La estructura de los JWT será comenzando por el Token para realizar los requests:

```json
{
  "sub": 5,
  "iat": 1559099479,
  "exp": 1559100079,
  "type": "bearer"
}
```

Estructura Token para hacer refresh:

```json
{
  "sub": 5,
  "iat": 1559099479,
  "exp": 1559100979,
  "type": "refresh"
}
```

El tiempo de duración del token de request será de 10 minutos y el tiempo de duración del token de refresh será de 25 minutos.

Los endpoints solicitarán el header HTTP `Authorization` y en el campo estará `Bearer` [JWT]

```http
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zb2Z0d2FyZTEudGVzdFwvYXBpXC9sb2dpbiIsImlhdCI6MTU1ODEwNTI4OSwiZXhwIjoxNTU4MTA4ODg5LCJuYmYiOjE1NTgxMDUyODksImp0aSI6Im93ZkVWcHVjd3dpY1dBZjAiLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.BtrPc6Q4S-ylstfdhXDchzCEsuwcha6OuJWZ6vllMX4
```

Si la solicitud falla se generará el siguiente error:

```json
{
    "Message": "Token Invalid",
    "Code": 400
}
```

Si se manda un token expirado se devolverá:

```json
{
    "Message": "Token Expired",
    "Code": 307
}
```
# Group Persons
Listar y registrar personas

## Listado de People [/Person]

### Get People [GET]
Recuperar el listado de todas las personas registradas

+ Parameters

    + id_person (required, bigInt, `154`) ... La llave primaria de la persona de tipo big integer, tiene una lógica autoincremental.
    + name (required, string, `John`) ... El nombre de la persona.
    + last_name (required, string, `Smith`) ... El apellido de la persona.
    + b_day (required, date, `1997-12-10` ) ... Fecha de nacimiento de la persona.
    + address (required, string, `Star Street, Illinois Avenue`) ... Dirección domiciliaria de la persona.
    + gender (required, string, `Male`) ... El género de la persona.
    + phone (required, int, `78936475`) ... Teléfono (domicilio o móvil) de la persona.
    + country (required, string, `España`) ... País donde se encuentra de la persona.
    + city (required, string, `Saragoza`) ... Ciudad donde se encuentra la persona.
    + e-mail (required, string, `john@smith.com`) ... Correo electrónico de la persona.
    + id_img (required, integer, `1`) ... El id de la imagen de perfil que tiene la persona (Para su visualización).
    + ID_NIT (required, integer, `2334408`) ... Número de Cédula de Idenditad o Nit de la empresa.

+ Response 200 (application/json)

        {
            "data": [
                {
                    "id_person": 154,
                    "name": "John",
                    "last_name": "Smith",
                    "b_day": "1997-12-10",
                    "phone": "78945123",
                    "gender": "Male",
                    "country": "España",
                    "city": "Saragoza",
                    "e-mail": "john@smith.com",
                    "id_img": 152,
                    "ID-NIT": 2334408 
                },
                {
                    "id_person": 12352,
                    "name": "Marianne",
                    "last_name": "Carow",
                    "b_day": "1997-06-10",
                    "phone": "89745633",
                    "gender": "Female",
                    "country": "Canadá",
                    "city": "Ottawa",
                    "e-mail": "mariane@carow.com",
                    "id_img": 110,
                    "ID-NIT": 4916388
                }
            ]
        }

+ Response 404 (application/json)

        {
          "Message": "No People Registered",
          "error_code": 404
        }

## Creación de persona [/Person]

### Post Person [POST]

+ Request (application/json)

      + Body

            {
              "name": "John",
              "last_name": "Smith",
              "b_day": "1997-12-10",
              "address": "Hello St, Hi Avenue",
              "phone": "78945123",
              "gender": "Male",
              "country": "España",
              "city": "Saragoza",
              "e-mail": "john@smith.com",
              "id_img": 152,
              "ID_NIT": 2334408         
            }
+ Response 201 (application/json)

        {
            "Message": "Person added",
            "code": 201
        }

+ Response 400 (application/json)

        {
            "Message": "Person not added",
            "code": 400
        }

+ Response 400 (application/json)

        {
            "Message": "Invalid Data",
            "code": 400
        }

## Búsqueda persona específica [/Person/{id_person}]

+ Parameters

    + id_person (required, integer, `154` ) ... La llave primaria de la persona, es un big int, y de lógica autoincremental

### Get Person [GET]

+ Response 200 (application/json)

        {
            "data": [
                {
                    "id_person": 154,
                    "name": "John",
                    "last_name": "Smith",
                    "b_day": "1997-12-10",
                    "phone": "78945123",
                    "gender": "Male",
                    "country": "España",
                    "city": "Saragoza",
                    "e-mail": "john@smith.com",
                    "id_img": 152,
                    "ID_NIT": 2334408 
                }
            ]
        }

+ Response 404 (application/json)

        {
          "Message": "Person not found",
          "error_code": 404
        }

## Eliminación Person específica [/Person/{id_person}]

Se eliminará a la persona junto con su usuario registrado

+ Parameters

    + id_person (required, bigInt, `154`) ... La llave primaria de la persona de tipo big integer, tiene una lógica autoincremental.

### Delete Person [DELETE]

+ Request 

    + Headers

            Authorization: Bearer [access token]
+ Response 200 (application/json)
        {
          "Message": "Delete Successful",
          "code": 200
        }

+ Response 400 (application/json)

        {
          "Message": "Delete Failed",
          "error_code": 400
        }


## Update de datos Person [/Person/{id_person}]

Se eliminará a la persona junto con su usuario registrado

+ Parameters

    + id_person (required, bigInt, `154`) ... La llave primaria de la persona de tipo big integer, tiene una lógica autoincremental.
    + name (required, string, `John`) ... El nombre de la persona.
    + last_name (required, string, `Smith`) ... El apellido de la persona.
    + b_day (required, date, `1997-12-10` ) ... Fecha de nacimiento de la persona.
    + address (required, string, `Star Street, Illinois Avenue`) ... Dirección domiciliaria de la persona.
    + gender (required, string, `Male`) ... El género de la persona.
    + phone (required, int, `78936475`) ... Teléfono (domicilio o móvil) de la persona.
    + country (required, string, `España`) ... País donde se encuentra de la persona.
    + city (required, string, `Saragoza`) ... Ciudad donde se encuentra la persona.
    + e-mail (required, string, `john@smith.com`) ... Correo electrónico de la persona.
    + id_img (required, integer, `1`) ... El id de la imagen de perfil que tiene la persona (Para su visualización).
    + ID_NIT (required, integer, `2334408`) ... Número de Cédula de Idenditad o Nit de la empresa.

### Put Person [PUT]

+ Request (application/json)

    + Headers

            Authorization: Bearer [access token]

    + Body

            {
              "name": "John",
              "last_name": "Smith",
              "b_day": "1997-12-10",
              "address": "Hello St, Hi Avenue",
              "phone": "78945123",
              "gender": "Male",
              "country": "España",
              "city": "Saragoza",
              "e-mail": "john@smith.com",
              "id_img": 152,
              "ID_NIT": 2334408         
            }
            
+ Response 200 (application/json)
        {
          "Message": "Person edited",
          "code": 200
        }

+ Response 404 (application/json)

        {
          "Message": "Person not edited",
          "error_code": 400
        }
# Group Users
Buscar y administrar usuarios.

## Listado de Usuarios [/Person/{id_person}/User]

### Get Users [GET]
Recuperar el listado de los usuarios registrados a nombre de una persona.

+ Parameters

    + id_person (optional, string, `1`) ... El id de persona la cual sólo debe tener registado un usuario a su nombre, por ende la respuesta tiene que ser un sólo objeto.

+ Response 200 (application/json)

        {
          "data": {
              "id_user": 1,
              "username": "j.smith",
              "email": "john@smith.com",
              "email_verified_at": null,
              "salt": "software1",
              "status": "active",
              "id_person": 1
          }
        }
+ Response 404 (application/json)

        {
          "Message": "User not found",
          "error_code": 404
        }

## Registro de User [/api/register]

### Post User [POST]

Registro de User

+ Request (application/json)
      + Body
            {
              "username": Rain Man,
              "email": "rain@man.com",
              "password": "secret",
              "password_confirmation": "secret",
              "id_person": "1",    
            }

+ Response 201 (application/json)
        {
            "Message": "User added",
            "Code": 201
        }
+ Response 400 (application/json)
        {
            "Message": "User not added",
            "Code": 400
        }
## Login de User [/api/login]

### Login User [POST]

Login de usuario

+ Request (application/json)

      + Body
            {
              "username": Rain Man,
              "password": "secret"  
            }

+ Response 201 (application/json)

        {
            "bearer token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zb2Z0d2FyZTEudGVzdFwvYXBpXC9yZWdpc3RlciIsImlhdCI6MTU1ODA1MzcyNywiZXhwIjoxNTU4MDU3MzI3LCJuYmYiOjE1NTgwNTM3MjcsImp0aSI6IktWSE1Nc1ozRFBocUNQY0wiLCJzdWIiOjMsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.8YqSvFnhF1ry15XvGRdbDCXhUlMcIO1fasN9XsmDaYE",
            "refresh token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zb2Z0d2FyZTEudGVzdFwvYXBpXC9yZWdpc3RlciIsImlhdCI6MTU1ODA1MzcyNywiZXhwIjoxNTU4MDU3MzI3LCJuYmYiOjE1NTgwNTM3MjcsImp0aSI6IktWSE1Nc1ozRFBocUNQY0wiLCJzdWIiOjMsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.8YqSvFnhF1ry15XvGRdbDCXhUlMcIO1fasN9XsmDaYE"
        }
+ Response 404 (application/json)

        {
            "Message": "Wrong Credentials",
            "Code": 404
        }
        
## User [/Person/{id_person}/User/{id_user}]

+ Parameters

    + id_person (required, integer) ... The unique identifier of a place
    + id_user (required, integer) ... The unique identifier of an user

### Get User [GET]
+ Request

    + Headers

            Authorization: Bearer {access token}
+ Response 200

{
    "data": {
        "id_user": 5,
        "username": "ryan.higa",
        "email": "ryan@higa.com",
        "email_verified_at": null,
        "password": "$2y$10$h8J0rku6hyCQ4/WRDJ27JeNLd/NHjv1wmPOmhj0jj9vO3V/MYci/W",
        "salt": "Software1",
        "status": "active",
        "id_person": 5,
        "remember_token": null,
        "deleted_at": null,
        "created_at": "2019-05-25 19:23:26",
        "updated_at": "2019-05-25 20:47:15"
    }
}
+ Response 404 (application/json)

        {
          "Message": "User not Found",
          "Code": 404
        }

### Modify User [PUT]

+ Request (application/json)

    + Headers

            Authorization: Bearer [access token]

    + Body

            {
              "username": "ryan.higa",
              "email": "ryan@higa.com",
              "password": "secret",
              "password_confirmation": "secret",
              "id_person": 5      
            }
+ Response 200 (application/json)

        {
          "Message": "Edit Successful",
          "Code": 200
        }
+ Response 500 (application/json)

        {
          "Message": "Edit Unsuccessful",
          "Code": 500
        }

### Delete User [DELETE]

+ Request

    + Headers

            Authorization: Bearer {access token}

+ Response 200

        {
          "Message": "Delete Successful",
          "Code": 200
        }
+ Response 500 (application/json)

        {
          "Message": "Delete Unsuccessful",
          "Code": 500
        }
