/*
 Navicat PostgreSQL Data Transfer

 Source Server         : DiegoDb
 Source Server Type    : PostgreSQL
 Source Server Version : 110000
 Source Host           : 10.144.15.208:5432
 Source Catalog        : software1
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110000
 File Encoding         : 65001

 Date: 15/05/2019 01:03:55
*/


-- ----------------------------
-- Sequence structure for activities_id_activity_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."activities_id_activity_seq";
CREATE SEQUENCE "public"."activities_id_activity_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for claims_id_claim_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."claims_id_claim_seq";
CREATE SEQUENCE "public"."claims_id_claim_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for deposits_id_deposit_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."deposits_id_deposit_seq";
CREATE SEQUENCE "public"."deposits_id_deposit_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for enterprise_deposits_id_enterprise_deposit_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."enterprise_deposits_id_enterprise_deposit_seq";
CREATE SEQUENCE "public"."enterprise_deposits_id_enterprise_deposit_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for enterprise_packages_id_enterprise_package_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."enterprise_packages_id_enterprise_package_seq";
CREATE SEQUENCE "public"."enterprise_packages_id_enterprise_package_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for enterprises_id_enterprise_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."enterprises_id_enterprise_seq";
CREATE SEQUENCE "public"."enterprises_id_enterprise_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for h__packages_id_h_package_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."h__packages_id_h_package_seq";
CREATE SEQUENCE "public"."h__packages_id_h_package_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for h__people_id_h_people_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."h__people_id_h_people_seq";
CREATE SEQUENCE "public"."h__people_id_h_people_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for h__users_id_h_user_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."h__users_id_h_user_seq";
CREATE SEQUENCE "public"."h__users_id_h_user_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for images_id_img_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."images_id_img_seq";
CREATE SEQUENCE "public"."images_id_img_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for locations_id_location_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."locations_id_location_seq";
CREATE SEQUENCE "public"."locations_id_location_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for migrations_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."migrations_id_seq";
CREATE SEQUENCE "public"."migrations_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for package__offers_id_package_offer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."package__offers_id_package_offer_seq";
CREATE SEQUENCE "public"."package__offers_id_package_offer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for package_images_id_package_image_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."package_images_id_package_image_seq";
CREATE SEQUENCE "public"."package_images_id_package_image_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for package_sales_id_package_sale_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."package_sales_id_package_sale_seq";
CREATE SEQUENCE "public"."package_sales_id_package_sale_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for packages_id_package_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."packages_id_package_seq";
CREATE SEQUENCE "public"."packages_id_package_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for people_id_person_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."people_id_person_seq";
CREATE SEQUENCE "public"."people_id_person_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for privileges_id_privilege_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."privileges_id_privilege_seq";
CREATE SEQUENCE "public"."privileges_id_privilege_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for role_privileges_id_role_privilege_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."role_privileges_id_role_privilege_seq";
CREATE SEQUENCE "public"."role_privileges_id_role_privilege_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for roles_id_role_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."roles_id_role_seq";
CREATE SEQUENCE "public"."roles_id_role_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tourists_id_tourist_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tourists_id_tourist_seq";
CREATE SEQUENCE "public"."tourists_id_tourist_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tranport__enterprises_id_transport_enterprise_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tranport__enterprises_id_transport_enterprise_seq";
CREATE SEQUENCE "public"."tranport__enterprises_id_transport_enterprise_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tranports_id_transport_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tranports_id_transport_seq";
CREATE SEQUENCE "public"."tranports_id_transport_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for transport__types_id_transport_type_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."transport__types_id_transport_type_seq";
CREATE SEQUENCE "public"."transport__types_id_transport_type_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for travels_id_travel_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."travels_id_travel_seq";
CREATE SEQUENCE "public"."travels_id_travel_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_roles_id_user_role_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_roles_id_user_role_seq";
CREATE SEQUENCE "public"."user_roles_id_user_role_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_user_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_user_seq";
CREATE SEQUENCE "public"."users_id_user_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for verifications_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."verifications_id_seq";
CREATE SEQUENCE "public"."verifications_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for verificators_id_verificator_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."verificators_id_verificator_seq";
CREATE SEQUENCE "public"."verificators_id_verificator_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for activities
-- ----------------------------
DROP TABLE IF EXISTS "public"."activities";
CREATE TABLE "public"."activities" (
  "id_activity" int8 NOT NULL DEFAULT nextval('activities_id_activity_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "date" date NOT NULL,
  "time_start" time(0) NOT NULL,
  "time_end" time(0) NOT NULL,
  "description" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "id_package" int8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for claims
-- ----------------------------
DROP TABLE IF EXISTS "public"."claims";
CREATE TABLE "public"."claims" (
  "id_claim" int8 NOT NULL DEFAULT nextval('claims_id_claim_seq'::regclass),
  "commentary" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "id_package_sale" int8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for deposits
-- ----------------------------
DROP TABLE IF EXISTS "public"."deposits";
CREATE TABLE "public"."deposits" (
  "id_deposit" int8 NOT NULL DEFAULT nextval('deposits_id_deposit_seq'::regclass),
  "bank" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "depositor" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "trans_num" int4 NOT NULL,
  "date" date NOT NULL,
  "id_img" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for enterprise_deposits
-- ----------------------------
DROP TABLE IF EXISTS "public"."enterprise_deposits";
CREATE TABLE "public"."enterprise_deposits" (
  "id_enterprise_deposit" int8 NOT NULL DEFAULT nextval('enterprise_deposits_id_enterprise_deposit_seq'::regclass),
  "id_deposit" int8 NOT NULL,
  "id_enterprise" int8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for enterprise_packages
-- ----------------------------
DROP TABLE IF EXISTS "public"."enterprise_packages";
CREATE TABLE "public"."enterprise_packages" (
  "id_enterprise_package" int8 NOT NULL DEFAULT nextval('enterprise_packages_id_enterprise_package_seq'::regclass),
  "id_package" int8 NOT NULL,
  "id_enterprise" int8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for enterprises
-- ----------------------------
DROP TABLE IF EXISTS "public"."enterprises";
CREATE TABLE "public"."enterprises" (
  "id_enterprise" int8 NOT NULL DEFAULT nextval('enterprises_id_enterprise_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "func_lic" int4 NOT NULL,
  "score" float8 NOT NULL,
  "id_user" int8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of enterprises
-- ----------------------------
INSERT INTO "public"."enterprises" VALUES (1, 'ROUTERISMO', 534132932, 5, 1, '2019-05-15 00:46:26', '2019-05-15 00:46:30', '2019-05-15 00:46:33');

-- ----------------------------
-- Table structure for h__packages
-- ----------------------------
DROP TABLE IF EXISTS "public"."h__packages";
CREATE TABLE "public"."h__packages" (
  "id_h_package" int8 NOT NULL DEFAULT nextval('h__packages_id_h_package_seq'::regclass),
  "id_package" int8 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "price" float8 NOT NULL,
  "start_date" date NOT NULL,
  "end_date" date NOT NULL,
  "max_adults" int4 NOT NULL,
  "max_children" int4 NOT NULL,
  "creation_date" date NOT NULL,
  "score" float8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for h__people
-- ----------------------------
DROP TABLE IF EXISTS "public"."h__people";
CREATE TABLE "public"."h__people" (
  "id_h_people" int8 NOT NULL DEFAULT nextval('h__people_id_h_people_seq'::regclass),
  "id_person" int8 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "last_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "b_day" date NOT NULL,
  "address" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "phone" int4 NOT NULL,
  "gender" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "country" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "city" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "e-mail" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "id_img" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "ID-NIT" int4 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of h__people
-- ----------------------------
INSERT INTO "public"."h__people" VALUES (1, 1, 'Jose', 'Perez', '1997-06-19', 'av. veloz', 413212, 'Male', 'Bolvia', 'La Paz', 'none@nome.rg', '1', 1, '2019-05-15 00:51:43', '2019-05-15 00:51:46', '2019-05-15 00:51:49');

-- ----------------------------
-- Table structure for h__users
-- ----------------------------
DROP TABLE IF EXISTS "public"."h__users";
CREATE TABLE "public"."h__users" (
  "id_h_user" int8 NOT NULL DEFAULT nextval('h__users_id_h_user_seq'::regclass),
  "id_user" int8 NOT NULL,
  "username" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "salt" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "status" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "id_person" int4 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of h__users
-- ----------------------------
INSERT INTO "public"."h__users" VALUES (1, 1, 'PEPE', '', '1', 1, '2019-05-15 00:50:08', '2019-05-15 00:50:11', '2019-05-15 00:50:14');

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS "public"."images";
CREATE TABLE "public"."images" (
  "id_img" int8 NOT NULL DEFAULT nextval('images_id_img_seq'::regclass),
  "url" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of images
-- ----------------------------
INSERT INTO "public"."images" VALUES (1, 'none.jpg', '2019-05-15 00:49:21', '2019-05-15 00:49:25', '2019-05-15 00:49:30');

-- ----------------------------
-- Table structure for locations
-- ----------------------------
DROP TABLE IF EXISTS "public"."locations";
CREATE TABLE "public"."locations" (
  "id_location" int8 NOT NULL DEFAULT nextval('locations_id_location_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS "public"."migrations";
CREATE TABLE "public"."migrations" (
  "id" int4 NOT NULL DEFAULT nextval('migrations_id_seq'::regclass),
  "migration" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "batch" int4 NOT NULL
)
;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO "public"."migrations" VALUES (1, '2019_05_14_173226_create_privileges_table', 1);
INSERT INTO "public"."migrations" VALUES (2, '2019_05_14_173236_create_roles_table', 1);
INSERT INTO "public"."migrations" VALUES (3, '2019_05_14_173258_create_role_privileges_table', 1);
INSERT INTO "public"."migrations" VALUES (4, '2019_05_14_173320_create_people_table', 1);
INSERT INTO "public"."migrations" VALUES (5, '2019_05_14_173344_create_users_table', 1);
INSERT INTO "public"."migrations" VALUES (6, '2019_05_14_173400_create_user_roles_table', 1);
INSERT INTO "public"."migrations" VALUES (7, '2019_05_14_173424_create_verificators_table', 1);
INSERT INTO "public"."migrations" VALUES (8, '2019_05_14_173447_create_verifications_table', 1);
INSERT INTO "public"."migrations" VALUES (9, '2019_05_14_173457_create_deposits_table', 1);
INSERT INTO "public"."migrations" VALUES (10, '2019_05_14_173531_create_enterprises_table', 1);
INSERT INTO "public"."migrations" VALUES (11, '2019_05_14_173555_create_enterprise_deposits_table', 1);
INSERT INTO "public"."migrations" VALUES (12, '2019_05_14_173619_create_tourists_table', 1);
INSERT INTO "public"."migrations" VALUES (13, '2019_05_14_173705_create_transport__types_table', 1);
INSERT INTO "public"."migrations" VALUES (14, '2019_05_14_173721_create_tranport__enterprises_table', 1);
INSERT INTO "public"."migrations" VALUES (15, '2019_05_14_173729_create_tranports_table', 1);
INSERT INTO "public"."migrations" VALUES (16, '2019_05_14_173742_create_locations_table', 1);
INSERT INTO "public"."migrations" VALUES (17, '2019_05_14_173803_create_packages_table', 1);
INSERT INTO "public"."migrations" VALUES (18, '2019_05_14_173824_create_enterprise_packages_table', 1);
INSERT INTO "public"."migrations" VALUES (19, '2019_05_14_173837_create_travels_table', 1);
INSERT INTO "public"."migrations" VALUES (20, '2019_05_14_173845_create_images_table', 1);
INSERT INTO "public"."migrations" VALUES (21, '2019_05_14_173859_create_package_images_table', 1);
INSERT INTO "public"."migrations" VALUES (22, '2019_05_14_173929_create_package_sales_table', 1);
INSERT INTO "public"."migrations" VALUES (23, '2019_05_14_173945_create_package__offers_table', 1);
INSERT INTO "public"."migrations" VALUES (24, '2019_05_14_173954_create_activities_table', 1);
INSERT INTO "public"."migrations" VALUES (25, '2019_05_14_174004_create_claims_table', 1);
INSERT INTO "public"."migrations" VALUES (26, '2019_05_14_174101_create_h__packages_table', 1);
INSERT INTO "public"."migrations" VALUES (27, '2019_05_14_174108_create_h__users_table', 1);
INSERT INTO "public"."migrations" VALUES (28, '2019_05_14_174115_create_h__people_table', 1);

-- ----------------------------
-- Table structure for package__offers
-- ----------------------------
DROP TABLE IF EXISTS "public"."package__offers";
CREATE TABLE "public"."package__offers" (
  "id_package_offer" int8 NOT NULL DEFAULT nextval('package__offers_id_package_offer_seq'::regclass),
  "start_date" timestamp(0) NOT NULL,
  "end_date" timestamp(0) NOT NULL,
  "price_percentage" float8 NOT NULL,
  "status" bool NOT NULL,
  "id_package" int8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for package_images
-- ----------------------------
DROP TABLE IF EXISTS "public"."package_images";
CREATE TABLE "public"."package_images" (
  "id_package_image" int8 NOT NULL DEFAULT nextval('package_images_id_package_image_seq'::regclass),
  "id_package" int8 NOT NULL,
  "id_img" int8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for package_sales
-- ----------------------------
DROP TABLE IF EXISTS "public"."package_sales";
CREATE TABLE "public"."package_sales" (
  "id_package_sale" int8 NOT NULL DEFAULT nextval('package_sales_id_package_sale_seq'::regclass),
  "raw_amount" float8 NOT NULL,
  "sale_date" date NOT NULL,
  "score" float8 NOT NULL,
  "comment" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "id_tourist" int8 NOT NULL,
  "id_package" int8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for packages
-- ----------------------------
DROP TABLE IF EXISTS "public"."packages";
CREATE TABLE "public"."packages" (
  "id_package" int8 NOT NULL DEFAULT nextval('packages_id_package_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "price" float8 NOT NULL,
  "start_date" date NOT NULL,
  "end_date" date NOT NULL,
  "max_adults" int4 NOT NULL,
  "max_children" int4 NOT NULL,
  "creation_date" date NOT NULL,
  "score" float8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for people
-- ----------------------------
DROP TABLE IF EXISTS "public"."people";
CREATE TABLE "public"."people" (
  "id_person" int8 NOT NULL DEFAULT nextval('people_id_person_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "last_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "b_day" date NOT NULL,
  "address" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "phone" int4 NOT NULL,
  "gender" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "country" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "city" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "e-mail" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "id_img" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "ID-NIT" int4 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of people
-- ----------------------------
INSERT INTO "public"."people" VALUES (1, 'Luis', 'Cespedes', '1993-06-30', 'Av. Siempre Viva', 241213, 'Male', 'Bolivia', 'La Paz', '', '1', 123, '2019-05-15 00:41:25', '2019-05-15 00:41:30', '2019-05-15 00:41:34');
INSERT INTO "public"."people" VALUES (2, 'Mauricio', 'Barriga', '2019-05-21', 'av.Rapida', 5432188, 'Male', 'Bolivia', 'Cochabamba', '', '1', 123, '2019-05-15 00:55:32', '2019-05-07 00:55:36', '2019-05-29 00:55:39');
INSERT INTO "public"."people" VALUES (3, 'Estefania', 'Pantoja', '1993-08-21', 'Ninguna Zona', 45432, 'Female', 'Bolivia', 'Santa Cruz', ' ', '1', 123, '2019-05-15 00:56:57', '2019-05-15 00:57:04', '2019-05-15 00:57:09');

-- ----------------------------
-- Table structure for privileges
-- ----------------------------
DROP TABLE IF EXISTS "public"."privileges";
CREATE TABLE "public"."privileges" (
  "id_privilege" int8 NOT NULL DEFAULT nextval('privileges_id_privilege_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for role_privileges
-- ----------------------------
DROP TABLE IF EXISTS "public"."role_privileges";
CREATE TABLE "public"."role_privileges" (
  "id_role_privilege" int8 NOT NULL DEFAULT nextval('role_privileges_id_role_privilege_seq'::regclass),
  "id_role" int8 NOT NULL,
  "id_privilege" int8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."roles";
CREATE TABLE "public"."roles" (
  "id_role" int8 NOT NULL DEFAULT nextval('roles_id_role_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for tourists
-- ----------------------------
DROP TABLE IF EXISTS "public"."tourists";
CREATE TABLE "public"."tourists" (
  "id_tourist" int8 NOT NULL DEFAULT nextval('tourists_id_tourist_seq'::regclass),
  "id_user" int8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of tourists
-- ----------------------------
INSERT INTO "public"."tourists" VALUES (1, 1, '2019-05-15 00:52:21', '2019-05-15 00:52:25', '2019-05-15 00:52:28');

-- ----------------------------
-- Table structure for tranport__enterprises
-- ----------------------------
DROP TABLE IF EXISTS "public"."tranport__enterprises";
CREATE TABLE "public"."tranport__enterprises" (
  "id_transport_enterprise" int8 NOT NULL DEFAULT nextval('tranport__enterprises_id_transport_enterprise_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of tranport__enterprises
-- ----------------------------
INSERT INTO "public"."tranport__enterprises" VALUES (1, 'FEDEX', '2019-05-14 00:28:03', '2019-05-15 00:25:09', '2019-05-15 00:25:28');
INSERT INTO "public"."tranport__enterprises" VALUES (2, 'BOA', '2019-05-15 00:34:50', '2019-05-15 00:34:54', '2019-05-15 00:35:01');

-- ----------------------------
-- Table structure for tranports
-- ----------------------------
DROP TABLE IF EXISTS "public"."tranports";
CREATE TABLE "public"."tranports" (
  "id_transport" int8 NOT NULL DEFAULT nextval('tranports_id_transport_seq'::regclass),
  "id_transport_type" int8 NOT NULL,
  "id_transport_enterprise" int8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of tranports
-- ----------------------------
INSERT INTO "public"."tranports" VALUES (1, 1, 1, '2019-05-15 00:30:38', '2019-05-14 00:30:44', '2019-05-15 00:30:52');
INSERT INTO "public"."tranports" VALUES (2, 1, 2, '2019-05-15 00:53:28', '2019-05-15 00:53:31', '2019-05-15 00:53:34');
INSERT INTO "public"."tranports" VALUES (3, 1, 1, '2019-05-08 00:53:57', '2019-05-23 00:54:01', '2019-05-03 00:54:04');

-- ----------------------------
-- Table structure for transport__types
-- ----------------------------
DROP TABLE IF EXISTS "public"."transport__types";
CREATE TABLE "public"."transport__types" (
  "id_transport_type" int8 NOT NULL DEFAULT nextval('transport__types_id_transport_type_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of transport__types
-- ----------------------------
INSERT INTO "public"."transport__types" VALUES (1, 'Vuelo', '2019-05-15 00:30:01', '2019-05-15 00:30:04', '2019-05-15 00:30:08');

-- ----------------------------
-- Table structure for travels
-- ----------------------------
DROP TABLE IF EXISTS "public"."travels";
CREATE TABLE "public"."travels" (
  "id_travel" int8 NOT NULL DEFAULT nextval('travels_id_travel_seq'::regclass),
  "duration" int8 NOT NULL,
  "distance" float8 NOT NULL,
  "final_travel" bool NOT NULL,
  "first_travel" bool NOT NULL,
  "id_location_start" int8 NOT NULL,
  "id_location_end" int8 NOT NULL,
  "id_transport" int8 NOT NULL,
  "id_package" int8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_roles";
CREATE TABLE "public"."user_roles" (
  "id_user_role" int8 NOT NULL DEFAULT nextval('user_roles_id_user_role_seq'::regclass),
  "id_user" int8 NOT NULL,
  "id_role" int8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id_user" int8 NOT NULL DEFAULT nextval('users_id_user_seq'::regclass),
  "username" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email_verified_at" timestamp(0),
  "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "salt" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "status" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "id_person" int4,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES (1, 'Pepito32', 'pepe@pepe.pe', '2019-05-23 00:44:51', '123456', '654321', '1', 1, '2019-05-15 00:45:12', '2019-05-15 00:45:16', '2019-05-15 00:45:19');

-- ----------------------------
-- Table structure for verifications
-- ----------------------------
DROP TABLE IF EXISTS "public"."verifications";
CREATE TABLE "public"."verifications" (
  "id" int8 NOT NULL DEFAULT nextval('verifications_id_seq'::regclass),
  "result" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "observation" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "date" date NOT NULL,
  "id_verificator" int8 NOT NULL,
  "id_user" int8 NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of verifications
-- ----------------------------
INSERT INTO "public"."verifications" VALUES (1, '1', 'Aceptado', '2019-05-15', 1, 1, '2019-05-15 00:44:12', '2019-05-15 00:44:16', '2019-05-15 00:44:20');
INSERT INTO "public"."verifications" VALUES (2, '2', 'Rechazado', '2019-05-15', 1, 1, '2019-05-15 00:59:00', '2019-05-10 00:59:03', '2019-05-06 00:59:08');
INSERT INTO "public"."verifications" VALUES (3, '3', 'Observado', '2019-05-14', 1, 1, '2019-05-15 01:00:03', '2019-05-09 01:00:06', '2019-06-06 01:00:10');

-- ----------------------------
-- Table structure for verificators
-- ----------------------------
DROP TABLE IF EXISTS "public"."verificators";
CREATE TABLE "public"."verificators" (
  "id_verificator" int8 NOT NULL DEFAULT nextval('verificators_id_verificator_seq'::regclass),
  "full_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "username" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "salt" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "deleted_at" timestamp(0) NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of verificators
-- ----------------------------
INSERT INTO "public"."verificators" VALUES (1, 'Jose Pineda', 'Jpineda', '12345', '54321', '2019-05-15 00:43:10', '2019-05-15 00:43:13', '2019-05-15 00:43:16');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."activities_id_activity_seq"
OWNED BY "public"."activities"."id_activity";
SELECT setval('"public"."activities_id_activity_seq"', 2, false);
ALTER SEQUENCE "public"."claims_id_claim_seq"
OWNED BY "public"."claims"."id_claim";
SELECT setval('"public"."claims_id_claim_seq"', 2, false);
ALTER SEQUENCE "public"."deposits_id_deposit_seq"
OWNED BY "public"."deposits"."id_deposit";
SELECT setval('"public"."deposits_id_deposit_seq"', 2, false);
ALTER SEQUENCE "public"."enterprise_deposits_id_enterprise_deposit_seq"
OWNED BY "public"."enterprise_deposits"."id_enterprise_deposit";
SELECT setval('"public"."enterprise_deposits_id_enterprise_deposit_seq"', 2, false);
ALTER SEQUENCE "public"."enterprise_packages_id_enterprise_package_seq"
OWNED BY "public"."enterprise_packages"."id_enterprise_package";
SELECT setval('"public"."enterprise_packages_id_enterprise_package_seq"', 2, false);
ALTER SEQUENCE "public"."enterprises_id_enterprise_seq"
OWNED BY "public"."enterprises"."id_enterprise";
SELECT setval('"public"."enterprises_id_enterprise_seq"', 2, false);
ALTER SEQUENCE "public"."h__packages_id_h_package_seq"
OWNED BY "public"."h__packages"."id_h_package";
SELECT setval('"public"."h__packages_id_h_package_seq"', 2, false);
ALTER SEQUENCE "public"."h__people_id_h_people_seq"
OWNED BY "public"."h__people"."id_h_people";
SELECT setval('"public"."h__people_id_h_people_seq"', 2, false);
ALTER SEQUENCE "public"."h__users_id_h_user_seq"
OWNED BY "public"."h__users"."id_h_user";
SELECT setval('"public"."h__users_id_h_user_seq"', 2, false);
ALTER SEQUENCE "public"."images_id_img_seq"
OWNED BY "public"."images"."id_img";
SELECT setval('"public"."images_id_img_seq"', 2, false);
ALTER SEQUENCE "public"."locations_id_location_seq"
OWNED BY "public"."locations"."id_location";
SELECT setval('"public"."locations_id_location_seq"', 2, false);
ALTER SEQUENCE "public"."migrations_id_seq"
OWNED BY "public"."migrations"."id";
SELECT setval('"public"."migrations_id_seq"', 29, true);
ALTER SEQUENCE "public"."package__offers_id_package_offer_seq"
OWNED BY "public"."package__offers"."id_package_offer";
SELECT setval('"public"."package__offers_id_package_offer_seq"', 2, false);
ALTER SEQUENCE "public"."package_images_id_package_image_seq"
OWNED BY "public"."package_images"."id_package_image";
SELECT setval('"public"."package_images_id_package_image_seq"', 2, false);
ALTER SEQUENCE "public"."package_sales_id_package_sale_seq"
OWNED BY "public"."package_sales"."id_package_sale";
SELECT setval('"public"."package_sales_id_package_sale_seq"', 2, false);
ALTER SEQUENCE "public"."packages_id_package_seq"
OWNED BY "public"."packages"."id_package";
SELECT setval('"public"."packages_id_package_seq"', 2, false);
ALTER SEQUENCE "public"."people_id_person_seq"
OWNED BY "public"."people"."id_person";
SELECT setval('"public"."people_id_person_seq"', 2, false);
ALTER SEQUENCE "public"."privileges_id_privilege_seq"
OWNED BY "public"."privileges"."id_privilege";
SELECT setval('"public"."privileges_id_privilege_seq"', 2, false);
ALTER SEQUENCE "public"."role_privileges_id_role_privilege_seq"
OWNED BY "public"."role_privileges"."id_role_privilege";
SELECT setval('"public"."role_privileges_id_role_privilege_seq"', 2, false);
ALTER SEQUENCE "public"."roles_id_role_seq"
OWNED BY "public"."roles"."id_role";
SELECT setval('"public"."roles_id_role_seq"', 2, false);
ALTER SEQUENCE "public"."tourists_id_tourist_seq"
OWNED BY "public"."tourists"."id_tourist";
SELECT setval('"public"."tourists_id_tourist_seq"', 2, false);
ALTER SEQUENCE "public"."tranport__enterprises_id_transport_enterprise_seq"
OWNED BY "public"."tranport__enterprises"."id_transport_enterprise";
SELECT setval('"public"."tranport__enterprises_id_transport_enterprise_seq"', 2, false);
ALTER SEQUENCE "public"."tranports_id_transport_seq"
OWNED BY "public"."tranports"."id_transport";
SELECT setval('"public"."tranports_id_transport_seq"', 2, false);
ALTER SEQUENCE "public"."transport__types_id_transport_type_seq"
OWNED BY "public"."transport__types"."id_transport_type";
SELECT setval('"public"."transport__types_id_transport_type_seq"', 2, false);
ALTER SEQUENCE "public"."travels_id_travel_seq"
OWNED BY "public"."travels"."id_travel";
SELECT setval('"public"."travels_id_travel_seq"', 2, false);
ALTER SEQUENCE "public"."user_roles_id_user_role_seq"
OWNED BY "public"."user_roles"."id_user_role";
SELECT setval('"public"."user_roles_id_user_role_seq"', 2, false);
ALTER SEQUENCE "public"."users_id_user_seq"
OWNED BY "public"."users"."id_user";
SELECT setval('"public"."users_id_user_seq"', 2, false);
ALTER SEQUENCE "public"."verifications_id_seq"
OWNED BY "public"."verifications"."id";
SELECT setval('"public"."verifications_id_seq"', 2, false);
ALTER SEQUENCE "public"."verificators_id_verificator_seq"
OWNED BY "public"."verificators"."id_verificator";
SELECT setval('"public"."verificators_id_verificator_seq"', 2, false);

-- ----------------------------
-- Primary Key structure for table activities
-- ----------------------------
ALTER TABLE "public"."activities" ADD CONSTRAINT "activities_pkey" PRIMARY KEY ("id_activity");

-- ----------------------------
-- Primary Key structure for table claims
-- ----------------------------
ALTER TABLE "public"."claims" ADD CONSTRAINT "claims_pkey" PRIMARY KEY ("id_claim");

-- ----------------------------
-- Primary Key structure for table deposits
-- ----------------------------
ALTER TABLE "public"."deposits" ADD CONSTRAINT "deposits_pkey" PRIMARY KEY ("id_deposit");

-- ----------------------------
-- Primary Key structure for table enterprise_deposits
-- ----------------------------
ALTER TABLE "public"."enterprise_deposits" ADD CONSTRAINT "enterprise_deposits_pkey" PRIMARY KEY ("id_enterprise_deposit");

-- ----------------------------
-- Primary Key structure for table enterprise_packages
-- ----------------------------
ALTER TABLE "public"."enterprise_packages" ADD CONSTRAINT "enterprise_packages_pkey" PRIMARY KEY ("id_enterprise_package");

-- ----------------------------
-- Primary Key structure for table enterprises
-- ----------------------------
ALTER TABLE "public"."enterprises" ADD CONSTRAINT "enterprises_pkey" PRIMARY KEY ("id_enterprise");

-- ----------------------------
-- Primary Key structure for table h__packages
-- ----------------------------
ALTER TABLE "public"."h__packages" ADD CONSTRAINT "h__packages_pkey" PRIMARY KEY ("id_h_package");

-- ----------------------------
-- Primary Key structure for table h__people
-- ----------------------------
ALTER TABLE "public"."h__people" ADD CONSTRAINT "h__people_pkey" PRIMARY KEY ("id_h_people");

-- ----------------------------
-- Primary Key structure for table h__users
-- ----------------------------
ALTER TABLE "public"."h__users" ADD CONSTRAINT "h__users_pkey" PRIMARY KEY ("id_h_user");

-- ----------------------------
-- Primary Key structure for table images
-- ----------------------------
ALTER TABLE "public"."images" ADD CONSTRAINT "images_pkey" PRIMARY KEY ("id_img");

-- ----------------------------
-- Primary Key structure for table locations
-- ----------------------------
ALTER TABLE "public"."locations" ADD CONSTRAINT "locations_pkey" PRIMARY KEY ("id_location");

-- ----------------------------
-- Primary Key structure for table migrations
-- ----------------------------
ALTER TABLE "public"."migrations" ADD CONSTRAINT "migrations_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table package__offers
-- ----------------------------
ALTER TABLE "public"."package__offers" ADD CONSTRAINT "package__offers_pkey" PRIMARY KEY ("id_package_offer");

-- ----------------------------
-- Primary Key structure for table package_images
-- ----------------------------
ALTER TABLE "public"."package_images" ADD CONSTRAINT "package_images_pkey" PRIMARY KEY ("id_package_image");

-- ----------------------------
-- Primary Key structure for table package_sales
-- ----------------------------
ALTER TABLE "public"."package_sales" ADD CONSTRAINT "package_sales_pkey" PRIMARY KEY ("id_package_sale");

-- ----------------------------
-- Primary Key structure for table packages
-- ----------------------------
ALTER TABLE "public"."packages" ADD CONSTRAINT "packages_pkey" PRIMARY KEY ("id_package");

-- ----------------------------
-- Primary Key structure for table people
-- ----------------------------
ALTER TABLE "public"."people" ADD CONSTRAINT "people_pkey" PRIMARY KEY ("id_person");

-- ----------------------------
-- Primary Key structure for table privileges
-- ----------------------------
ALTER TABLE "public"."privileges" ADD CONSTRAINT "privileges_pkey" PRIMARY KEY ("id_privilege");

-- ----------------------------
-- Primary Key structure for table role_privileges
-- ----------------------------
ALTER TABLE "public"."role_privileges" ADD CONSTRAINT "role_privileges_pkey" PRIMARY KEY ("id_role_privilege");

-- ----------------------------
-- Primary Key structure for table roles
-- ----------------------------
ALTER TABLE "public"."roles" ADD CONSTRAINT "roles_pkey" PRIMARY KEY ("id_role");

-- ----------------------------
-- Primary Key structure for table tourists
-- ----------------------------
ALTER TABLE "public"."tourists" ADD CONSTRAINT "tourists_pkey" PRIMARY KEY ("id_tourist");

-- ----------------------------
-- Primary Key structure for table tranport__enterprises
-- ----------------------------
ALTER TABLE "public"."tranport__enterprises" ADD CONSTRAINT "tranport__enterprises_pkey" PRIMARY KEY ("id_transport_enterprise");

-- ----------------------------
-- Primary Key structure for table tranports
-- ----------------------------
ALTER TABLE "public"."tranports" ADD CONSTRAINT "tranports_pkey" PRIMARY KEY ("id_transport");

-- ----------------------------
-- Primary Key structure for table transport__types
-- ----------------------------
ALTER TABLE "public"."transport__types" ADD CONSTRAINT "transport__types_pkey" PRIMARY KEY ("id_transport_type");

-- ----------------------------
-- Primary Key structure for table travels
-- ----------------------------
ALTER TABLE "public"."travels" ADD CONSTRAINT "travels_pkey" PRIMARY KEY ("id_travel");

-- ----------------------------
-- Primary Key structure for table user_roles
-- ----------------------------
ALTER TABLE "public"."user_roles" ADD CONSTRAINT "user_roles_pkey" PRIMARY KEY ("id_user_role");

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_email_unique" UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id_user");

-- ----------------------------
-- Primary Key structure for table verifications
-- ----------------------------
ALTER TABLE "public"."verifications" ADD CONSTRAINT "verifications_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table verificators
-- ----------------------------
ALTER TABLE "public"."verificators" ADD CONSTRAINT "verificators_pkey" PRIMARY KEY ("id_verificator");

-- ----------------------------
-- Foreign Keys structure for table activities
-- ----------------------------
ALTER TABLE "public"."activities" ADD CONSTRAINT "activities_id_package_foreign" FOREIGN KEY ("id_package") REFERENCES "public"."packages" ("id_package") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table claims
-- ----------------------------
ALTER TABLE "public"."claims" ADD CONSTRAINT "claims_id_package_sale_foreign" FOREIGN KEY ("id_package_sale") REFERENCES "public"."package_sales" ("id_package_sale") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table enterprise_deposits
-- ----------------------------
ALTER TABLE "public"."enterprise_deposits" ADD CONSTRAINT "enterprise_deposits_id_deposit_foreign" FOREIGN KEY ("id_deposit") REFERENCES "public"."deposits" ("id_deposit") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."enterprise_deposits" ADD CONSTRAINT "enterprise_deposits_id_enterprise_foreign" FOREIGN KEY ("id_enterprise") REFERENCES "public"."enterprises" ("id_enterprise") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table enterprise_packages
-- ----------------------------
ALTER TABLE "public"."enterprise_packages" ADD CONSTRAINT "enterprise_packages_id_enterprise_foreign" FOREIGN KEY ("id_enterprise") REFERENCES "public"."enterprises" ("id_enterprise") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."enterprise_packages" ADD CONSTRAINT "enterprise_packages_id_package_foreign" FOREIGN KEY ("id_package") REFERENCES "public"."packages" ("id_package") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table enterprises
-- ----------------------------
ALTER TABLE "public"."enterprises" ADD CONSTRAINT "enterprises_id_user_foreign" FOREIGN KEY ("id_user") REFERENCES "public"."users" ("id_user") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table package__offers
-- ----------------------------
ALTER TABLE "public"."package__offers" ADD CONSTRAINT "package__offers_id_package_foreign" FOREIGN KEY ("id_package") REFERENCES "public"."packages" ("id_package") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table package_images
-- ----------------------------
ALTER TABLE "public"."package_images" ADD CONSTRAINT "package_images_id_img_foreign" FOREIGN KEY ("id_img") REFERENCES "public"."images" ("id_img") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."package_images" ADD CONSTRAINT "package_images_id_package_foreign" FOREIGN KEY ("id_package") REFERENCES "public"."packages" ("id_package") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table package_sales
-- ----------------------------
ALTER TABLE "public"."package_sales" ADD CONSTRAINT "package_sales_id_package_foreign" FOREIGN KEY ("id_package") REFERENCES "public"."packages" ("id_package") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."package_sales" ADD CONSTRAINT "package_sales_id_tourist_foreign" FOREIGN KEY ("id_tourist") REFERENCES "public"."tourists" ("id_tourist") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table role_privileges
-- ----------------------------
ALTER TABLE "public"."role_privileges" ADD CONSTRAINT "role_privileges_id_privilege_foreign" FOREIGN KEY ("id_privilege") REFERENCES "public"."privileges" ("id_privilege") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."role_privileges" ADD CONSTRAINT "role_privileges_id_role_foreign" FOREIGN KEY ("id_role") REFERENCES "public"."roles" ("id_role") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table tourists
-- ----------------------------
ALTER TABLE "public"."tourists" ADD CONSTRAINT "tourists_id_user_foreign" FOREIGN KEY ("id_user") REFERENCES "public"."users" ("id_user") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table tranports
-- ----------------------------
ALTER TABLE "public"."tranports" ADD CONSTRAINT "tranports_id_transport_enterprise_foreign" FOREIGN KEY ("id_transport_enterprise") REFERENCES "public"."tranport__enterprises" ("id_transport_enterprise") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."tranports" ADD CONSTRAINT "tranports_id_transport_type_foreign" FOREIGN KEY ("id_transport_type") REFERENCES "public"."transport__types" ("id_transport_type") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table travels
-- ----------------------------
ALTER TABLE "public"."travels" ADD CONSTRAINT "travels_id_location_end_foreign" FOREIGN KEY ("id_location_end") REFERENCES "public"."locations" ("id_location") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."travels" ADD CONSTRAINT "travels_id_location_start_foreign" FOREIGN KEY ("id_location_start") REFERENCES "public"."locations" ("id_location") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."travels" ADD CONSTRAINT "travels_id_package_foreign" FOREIGN KEY ("id_package") REFERENCES "public"."packages" ("id_package") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."travels" ADD CONSTRAINT "travels_id_transport_foreign" FOREIGN KEY ("id_transport") REFERENCES "public"."tranports" ("id_transport") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table user_roles
-- ----------------------------
ALTER TABLE "public"."user_roles" ADD CONSTRAINT "user_roles_id_role_foreign" FOREIGN KEY ("id_role") REFERENCES "public"."roles" ("id_role") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_roles" ADD CONSTRAINT "user_roles_id_user_foreign" FOREIGN KEY ("id_user") REFERENCES "public"."users" ("id_user") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_id_person_foreign" FOREIGN KEY ("id_person") REFERENCES "public"."people" ("id_person") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table verifications
-- ----------------------------
ALTER TABLE "public"."verifications" ADD CONSTRAINT "verifications_id_user_foreign" FOREIGN KEY ("id_user") REFERENCES "public"."users" ("id_user") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."verifications" ADD CONSTRAINT "verifications_id_verificator_foreign" FOREIGN KEY ("id_verificator") REFERENCES "public"."verificators" ("id_verificator") ON DELETE NO ACTION ON UPDATE NO ACTION;
